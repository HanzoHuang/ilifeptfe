<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Input;
use Session;
use Request;
use Socialite;
use App\User;
use Validator;

class AuthController extends Controller
{
    protected $redirectPath = 'Member';
    protected $backLoginRoute  = 'BackLogin';  // 後台登入頁
    protected $backHomeRoute   = 'BackHome';   // 後台首頁
    protected $homeRoute       = 'Member';       // 前台首頁
    protected $loginRoute      = 'RLogin';      // 前台登入頁
    protected $oauthLoginRoute = 'OAuthLogin/{type}';       // 第三方登入頁
    protected $vaildOAuthType  = ['facebook', 'google'];    // 有效的第三方登入類型
    
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
    public function authenticate(Request $request)
    {
        $isBackLogin = preg_match('/'.$this->backLoginRoute.'$/i', Request::server('HTTP_REFERER'));
        // 檢查有沒有登入
        if (Auth::check()) {
            $User = Auth::user();
            if ($isBackLogin && $User->type == 'admin') {
                return redirect()->intended($this->backHomeRoute);  
            }
            return redirect()->intended($this->homeRoute);
        }
        // 判斷登入來源是前台或後台，並作對應處理
        if ($isBackLogin) {
            // 後台認證
            if (Auth::attempt(['account' => Input::get('email', ''), 'password' => Input::get('password', ''), 'type' => 'admin'])) {
                // 撈取會員資料
                $User = Auth::user();

                //JOIN users,role,program_registration,role_controls 進行menu組合
                $results = DB::select('select menu_class.font_awesome,acl.sort,menu_class.sort,acl.name as acl_name ,menu_class.name,program_name from
                (SELECT program_registration.sort,role_id,program_registration_id,menu_class_id,program_registration.id,name,
                    program_name FROM `role_controls`
                    join
                    (select sort,menu_class_id,id,name,program_name
                    from program_registration where deleted_at is null and menu = "Y"
                     order by menu_class_id asc) as program_registration
                     on
                (program_registration.id=role_controls.program_registration_id)) as acl left join menu_class
                on(acl.menu_class_id=menu_class.id) where role_id=' . $User->role_id . ' order
                by menu_class.sort,acl.sort asc');
                //角色有註冊程式
                if (!empty($results)) {
                    foreach ($results as $key => $value) {
                        $NewData[$value->name][] = $results[$key];
                    }
                    Session::put('Menu', $NewData);

                //角色無註冊任何程式
                } else {
                    Session::put('Menu', 1);
                }
                return redirect()->intended($this->backHomeRoute);
                // 認證失敗...攜帶Flash Data過去
            } else {
                return redirect()->intended($this->backLoginRoute)->with('message', '帳號或密碼錯誤！');
            }
        } else {
            // 前台認證
            if (Auth::attempt(['account' => Input::get('email',''), 'password' => Input::get('password', '')])) {
                // 撈取會員資料
                $User = Auth::user();
                if(Session::has('tour_id')){
                    return redirect()->to('SignUp'.'/'.Session::get('tour_id'));
                }
                return redirect()->intended($this->homeRoute);
                // 認證失敗...攜帶Flash Data過去
            } else {

                return redirect()->intended($this->loginRoute)->with('message', '帳號或密碼錯誤！');
            }
        }
    }
    
    // 第三方登入導向
    public function oauthLogin($type)
    {
        $type = strtolower($type);
        return Socialite::driver($type)->redirect();
    }
    
    // 第三方登入處理
    public function oauthCallback($type)
    {
        $type = strtolower($type);
        
        // 檢查第三方登入，如果不是有效類型就導回首頁
        if (!in_array($type, $this->vaildOAuthType)) {
            return redirect($this->homeRoute);  
        }
        
        try {
            $user = Socialite::driver($type)->user();
        } catch (Exception $e) {
            return redirect(str_replace('{type}', $type, $this->oauthLoginRoute));
        }

        $authUser = User::where('oauth_id', $user->id)
            ->where('oauth_type', $type)
            ->whereNull('deleted_at')
            ->first();
 
        if (!$authUser) {
            $authUser = User::create([
                'account' => $type.'_'.$user->id,
                'name' => $user->name,
                'email' => $user->email,
                'oauth_type' => $type,
                'oauth_id' => $user->id,
            ]);   
        }
        
        Auth::login($authUser, true);
        return redirect($this->homeRoute);
    }

    public function validator($request)
    {
        $messages=[
            'email.required'  => '信箱是必填的',
            'email.required'  => '信箱格式錯誤',
            'email.unique'  => '該信箱已經註冊過了',
            'password.required'  => '密碼是必填的',
            'password.confirmed'  => '密碼不正確，請重新輸入',

        ];
        return $validator = Validator::make($request, [
             'email' => 'required|email|unique:users,email',
             'password' => 'required|confirmed',
        ],$messages);
    }
    public function create($user){
        return User::create([
            'account' => $user['email'],
            'email' => $user['email'],
            'password' => bcrypt($user['password']),
            'type' => 'user',
        ]);
    }
    

}
