<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Log;
use Auth;
use Session;
use App;
use Input;
abstract class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	public function __construct() {
		$lang=Session::get('lang','zh');
		Session::put('lang',$lang);	
		App::setLocale($lang);
	}
}
