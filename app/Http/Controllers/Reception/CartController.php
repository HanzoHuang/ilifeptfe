<?php

namespace App\Http\Controllers\Reception;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Mail;
use UploadHandler;
use DB;
use Vsmoraes\Pdf\Pdf;
use Excel;
use Session;
use Hash;
// use App\Support\SSCart;
use App\Http\Requests\MemberUpdateRequest;

class CartController extends Controller
{
    function __construct(Pdf $pdf) {
        if(!Session::has('lang')){
          Session::put('lang','zh');
        }
    }  
    protected $ModelName="App\User";
    protected $ProductTable="product";
    protected $ProductSizeTable="product_size";
    // 首頁
    public function AddCart()
    {
        // Session::clear();
        $data=Session::get('cart');
        //檢查購物車裡有無商品
        if(isset($data[Input::get('product_id')])){
            $data[Input::get('product_id')]['quantity']+=Input::get('quantity');
        }else{
            $data[Input::get('product_id')]=array('product_id'=>Input::get('product_id'),'quantity'=>(int)Input::get('quantity'),'price'=>Input::get('price'));
        }
        Session::put('cart',$data);
        return count($data);
    } 
    public function DelCart()
    {
        $data=Session::get('cart');
        unset($data[Input::get('product_id')]);
        Session::put('cart',$data);
        $this->TotelData();
        return json_encode($this->TotelData());
        
    }  
    public function EditQuantity()
    {
        $type=Input::get('type');
        $product_id=Input::get('product_id');
        $data=Session::get('cart');
        switch ($type) {
            case '1':
                $data[$product_id]['quantity']+=1;
                break;
            case '2':
                $data[$product_id]['quantity']-=1;
                break;
            default:
                break;
        }
        Session::put('cart',$data);
        return json_encode($this->TotelData($product_id));
    }
    public function TotelData($product_id=0)
    {
        $cart=Session::get('cart');
        $data=array();
        if($product_id!=0){
            $data['this_price']=$cart[$product_id]['price']*$cart[$product_id]['quantity'];
        }
        $data['total']=0;
        foreach($cart as $key =>$value){
            $data['total']+=$value['price']*$value['quantity'];
        }
        return $data;
    }
}