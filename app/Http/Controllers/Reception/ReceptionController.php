<?php

namespace App\Http\Controllers\Reception;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Mail;
use UploadHandler;
use DB;
use Vsmoraes\Pdf\Pdf;
use Excel;
use Session;
use Hash;
use App;
use App\Http\Requests\MemberUpdateRequest;

class ReceptionController extends Controller
{
    function __construct(Pdf $pdf) {
        if(!Session::has('lang')){
          Session::put('lang','zh');
        }
        App::setLocale(Session::get('lang','zh'));
        $cart_count=count(Session::get('cart',array()));
        view()->share('cart_count',$cart_count);
    }  
    protected $ModelName="App\User";
    protected $ProductTable="product";
    protected $ProductSizeTable="product_size";
    // 首頁
    public function index()
    {
        $lang=Session::get('lang');
        $index_config=db::Table('index_config')
        ->select(
            'banner_img',
            'about_img',
            $lang.'_about_content as about_content',
            'introduction_img1',
            $lang.'_introduction_content1 as introduction_content1',
            'introduction_img2',
            $lang.'_introduction_content2 as introduction_content2'
        )
        ->first();
        $news=db::Table('news')
            ->select('id','img','date',$lang.'_title as title',$lang.'_content as content')
            ->orderBy('sort','asc')
            ->where('type',1)
            ->take(4)
            ->whereNull('deleted_at')
            ->get();
        return view('reception.index',['index_config'=>$index_config,'news'=>$news]);
    }
    // 關於我們
    public function About()
    {
        $lang=Session::get('lang');
        $about_banner=db::Table('about_banner')->first();
        $about_type1=db::table('about')
            ->select('id',
                $lang.'_title as title',
                $lang.'_content as content',
                'icon'
            )
            ->orderBy('sort','asc')
            ->whereNull('deleted_at')
            ->where('type',1)
            ->get();

        $about_type2=db::table('about')
            ->select('id',
                $lang.'_title as title',
                $lang.'_content as content',
                'icon'
            )
            ->orderBy('sort','asc')
            ->whereNull('deleted_at')
            ->where('type',2)
            ->get();
        return view('reception.about',['about_type1'=>$about_type1,'about_type2'=>$about_type2,'about_banner'=>$about_banner]);
    }
    // 最新消息
    public function News()
    {
        $lang=Session::get('lang');
        $news_type1=db::table('news')
            ->select(
                'id',
                'date',
                'img',
                $lang.'_title as title',
                $lang.'_content as content'
            )
            ->orderBy('sort','asc')
            ->whereNull('deleted_at')
            ->where('type',1)
            ->paginate(10);
            $news_type2=db::table('news')
            ->select(
                'id',
                'date',
                'img',
                $lang.'_title as title',
                $lang.'_content as content'
            )
            ->orderBy('sort','asc')
            ->whereNull('deleted_at')
            ->where('type',2)
            ->paginate(10);
        return view('reception.news',['news_type1'=>$news_type1,'news_type2'=>$news_type2]);
    }
    // 最新消息detail
    public function NewsDetail($id)
    {
        $lang=Session::get('lang');
        $news=db::table('news')
            ->select('id',$lang.'_title as title',$lang.'_content as content','img','date')
            ->where('id',$id)
            ->first();
       return view('reception.news_detail',['news'=>$news]);
    }
    // 聯絡我們
    public function Ptfe()
    {
        $lang=Session::get('lang');
        $ptfe=db::Table('ptfe')
            ->select('id',$lang.'_title1 as title1',$lang.'_title2 as title2',$lang.'_content as content','img')
            ->orderBy('sort')
            ->whereNull('deleted_at')
            ->get();
        return view('reception.ptfe',['ptfe'=>$ptfe]);
    }
    public function PtfeDetail($id)
    {
        $lang=Session::get('lang');
        $previous_ptfe=db::Table('ptfe')->select('id')->where('id','<',$id)->orderBy('id','desc')->first();
        $ptfe=db::Table('ptfe')
            ->select('id',$lang.'_title1 as title1',$lang.'_title2 as title2',$lang.'_content as content','img')
            ->where('id',$id)
            ->whereNull('deleted_at')
            ->first();
        $next_ptfe=db::Table('ptfe')->select('id')->orderBy('id','asc')->where('id','>',$id)->first();
        return view('reception.ptfe_detail',['ptfe'=>$ptfe,'previous_ptfe'=>$previous_ptfe,'next_ptfe'=>$next_ptfe]);
    }
    public function Product()
    {
        $lang=Session::get('lang');
        $product=db::Table('product')
        ->select(
            $this->ProductTable.'.id',
            $this->ProductTable.'.'.$lang.'_name as name',
            $this->ProductTable.'.img1'
        )
        ->orderBy('sort','asc')
        ->take(5)
        ->get();

       return view('reception.product',['product'=>$product]);
    }
    public function ProductList()
    {
        $lang=Session::get('lang');
        $product=db::Table('product')
        ->select(
            $this->ProductTable.'.id',
            $this->ProductTable.'.'.$lang.'_name as name',
            $this->ProductTable.'.img1',
            $this->ProductTable.'.price',
            $this->ProductSizeTable.'.name as size'
        )
        ->join($this->ProductSizeTable,$this->ProductTable.'.product_size','=',$this->ProductSizeTable.'.id')
        ->orderBy($this->ProductTable.'.sort','asc')
        ->get();
       return view('reception.product_list',['product'=>$product]);
    }
    public function ProductDetail($id)
    {
        $lang=Session::get('lang');
        $product=db::Table('product')
        ->select(
            $this->ProductTable.'.id',
            $this->ProductTable.'.'.$lang.'_name as name',
            $this->ProductTable.'.'.$lang.'_content as content',
            $this->ProductTable.'.img1',
            $this->ProductTable.'.img2',
            $this->ProductTable.'.img3',
            $this->ProductTable.'.img4',
            $this->ProductTable.'.img5',
            $this->ProductTable.'.img6'
        )
        ->where('id',$id)
        ->first();
        return view('reception.product_detail',['product'=>$product]);
    }
    public function QA()
    {
        $lang=Session::get('lang');
        $qa_banner=db::table('qa_banner')->first();
        $qa=db::Table('qa')->select($lang.'_question as question',$lang.'_answer as answer')->orderBy('sort','asc')->whereNull('deleted_at')->get();
        return view('reception.qa',['qa_banner'=>$qa_banner,'qa'=>$qa]);
    }
    public function Contacts()
    {
        $lang=Session::get('lang');
        $contact_config=db::table('contact_config')
                        ->select($lang.'_address as address','tel','fax')
                        ->first();
        return view('reception.contacts',['contact_config'=>$contact_config]);
    }
    public function Cart()
    {
        $lang=Session::get('lang');
        $cart=Session::get('cart');
        $total=0;
        if(count($cart)==0){
            return redirect()->back();
        }
        foreach($cart as $key =>$value){
            $product=db::table('product')
            ->select('product.id','product.'.$lang.'_name as name','product.price','product.img1','product_size.name as size')
            ->join('product_size','product.product_size','=','product_size.id')
            ->where('product.id',$key)
            ->first();
            $cart[$key]['name']=$product->name;
            $cart[$key]['price']=$product->price;
            $cart[$key]['img']=$product->img1;
            $cart[$key]['size']=$product->size;
            $total+=$cart[$key]['price']*$cart[$key]['quantity'];
        }
        return view('reception.cart',['cart'=>$cart,'total'=>$total]);
    }
    public function Checkout()
    {
        $lang=Session::get('lang');
        $cart=Session::get('cart');
        if(!Session::has('member')){
            return redirect()->to('LoginPage');
        }
        $total=0;
        foreach($cart as $key =>$value){
             $product=db::table('product')
            ->select('product.id','product.'.$lang.'_name as name','product.price','product.img1','product_size.name as size')
            ->join('product_size','product.product_size','=','product_size.id')
            ->where('product.id',$key)
            ->first();
            $cart[$key]['name']=$product->name;
            $cart[$key]['price']=$product->price;
            $cart[$key]['img']=$product->img1;
            $cart[$key]['size']=$product->size;
            $total+=$cart[$key]['price']*$cart[$key]['quantity'];
        }
       return view('reception.checkout',['cart'=>$cart,'total'=>$total]);
    }
    public function Done(request $request)
    {
        // dd($request->all());
        if(!Session::has('cart')){
            return redirect()->to('Home');
        }
        if(!Session::has('member')){
            return redirect()->to('LoginPage');
        }
        $lang=Session::get('lang');
        $order=$request->all();
        unset($order['_token']);
        $order['no']=Date('Ymd').rand('11111','99999');
        $order['user_id']=Session::get('member')->id;
        $order_detail=array();
        $total=0;
        $cart=Session::pull('cart');
        foreach($cart as $key =>$value){
            $total+=$cart[$key]['price']*$cart[$key]['quantity'];
        }
        $order['total']=$total;
        $order['freight']=160;
        $order_id=db::table('order')->insertGetId($order);
        foreach($cart as $key =>$value){
            //order_detail
            $order_detail[]=array('order_id'=>$order_id,'product_id'=>$key,'quantity'=>$value['quantity'],'user_id'=>Session::get('member')->id);

            $product=db::table('product')
            ->select('product.id','product.'.$lang.'_name as name','product.price','product.img1','product_size.name as size')
            ->join('product_size','product.product_size','=','product_size.id')
            ->where('product.id',$key)
            ->first();
            $cart[$key]['name']=$product->name;
            $cart[$key]['price']=$product->price;
            $cart[$key]['img']=$product->img1;
            $cart[$key]['size']=$product->size;
        }
        // dd($order_detail);
        db::table('order_detail')->insert($order_detail);
        return view('reception.done',['order'=>$order,'cart'=>$cart]);
    }
    public function Forgot()
    {
       return view('reception.forgot');
    }
    public function LoginPage()
    {
        if(Session::has('member')){
            return redirect()->to('Member');
        }
        return view('reception.login_page');
    }
    public function Member()
    {
        if(!Session::has('member')){
            return redirect()->route('Reception.LoginPage');
        }
        $order=db::table('order')->where('user_id',Session::get('member')->id)->get();
       return view('reception.member',['order'=>$order]);
    }
    
    public function OrderDetail($id)
    {
        if(!Session::has('member')){
            return redirect()->route('Reception.LoginPage');
        }
        $lang=Session::get('lang');
        $order=db::table('order')->find($id);
        $order_detail=db::table('order_detail')
            ->select(
                'order_detail.id',
                'product.'.$lang.'_name as name',
                'order_detail.quantity',
                'product_size.name as size',
                'product.price',
                'product.img1'
            )
            ->join('order','order_detail.order_id','=','order.id')
            ->join('product','order_detail.product_id','=','product.id')
            ->join('product_size','product.product_size','=','product_size.id')
            ->where('order_detail.order_id',$id)
            ->where('order.user_id',Session::get('member')->id)
            ->get();

        return view('reception.order_detail',['order'=>$order,'order_detail'=>$order_detail]);
    }
    public function Registar()
    {
       return view('reception.registar');
    }
    public function ChangeLang()
    {
        if(Session::get('lang')=='zh'){
            Session::put('lang','en');
        }else{
            Session::put('lang','zh');
        }
    }
    public function RegistarPost(Request $request)
    {
        if($request->password==$request->ck_password){
            User::create(RequestForeach($request->all(),''));
            return redirect()->to('Member');
        }else{
            return redirect()->back()->withInput()->with('status','確認密碼與密碼不符');
        }
    }
    public function RLogin(Request $request)
    {
        $member=db::table('users')
        ->where('users.account',$request->account)
        ->first();
        if ($member) {
            if(!Hash::check($request->password, $member->password)){
                return redirect()->to(route('Reception.LoginPage'))->with('status','密碼錯誤');
            }
            Session::put('member',$member);
            return redirect()->to(route('Reception.Member'));
        }else{
            return redirect()->to(route('Reception.LoginPage'))->with('status','無此帳號');
        }
    }
    public function RLogout()
    {
        // 設定目前的頁面
        Session::clear();
        return redirect()->route('Reception.LoginPage');
    }

    public function MemberDataUpdate(MemberUpdateRequest $request)
    {
        $member=db::table('users')
        ->where('users.account',$request->account)
        ->first();
        $data=$request->all();
        unset($data['_token']);
        unset($data['old_password']);
        unset($data['ck_password']);
        if(!Hash::check($request->old_password,$member->password)){
            return redirect()->back()->withInput()->with('status','舊密碼錯誤');
        }
        User::where('id',$member->id)->update(RequestForeach($data,''));
        return redirect()->route('Reception.Member')->with('status','修改成功');
    }
}