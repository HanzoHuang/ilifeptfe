<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Order\Order;
use Auth;
use Redirect;
use Session;



class BackLoginController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return Redirect::to('BackHome');
        } else {
            return view('admin.BackLogin.index');
        }
    }
    public function logout()
    {
        Auth::logout();
        Session::forget('BackLogin');
        Session::has('Menu');
        return Redirect::to('BackLogin');
    }
    public function Home()
    {
        return view('admin.BackHome.index', [ 'LevelOne' => '首頁', 'LevelTwo' => '公告']);
    }
}
