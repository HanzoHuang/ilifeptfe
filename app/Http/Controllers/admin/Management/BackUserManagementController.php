<?php

namespace App\Http\Controllers\admin\Management;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use DB;

class BackUserManagementController extends BackController {
    //配合route resource
    protected $RouteName="BackUserManagement";
    //檔案路徑
    protected $Path="Upload/User/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\User";

    //主要table
    protected $Table1="users";
    //關聯table
    protected $Table2="roles";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '會員姓名', 'width' => '9'),
            array('title' => '會員帳號', 'width' => '9'),
            array('title' => '會員信箱', 'width' => '9'),
            array('title' => '角色', 'width' => '9'),
            array('title' => '下架時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)->join($this->Table2,$this->Table2.'.id','=',$this->Table1.'.role_id')
        ->select($this->Table1.'.id as id',$this->Table1.'.name as name','account','email',$this->Table2.'.name as role_name',$this->Table1.'.deleted_at')->get();
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "會員新增";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "會員修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'SelectValue'=>$data->product_class_id,'SelectValue2'=>json_encode(explode(',',$data->product_specification_id))]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        $Table2 = DB::table($this->Table2)
            ->whereNull('deleted_at')
            ->select('id', 'name')
            ->get();
        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'name', 'name' => 'name', 'CNname' => '姓名：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : null),
            array('id' => 'account', 'name' => 'account', 'CNname' => '帳號：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['account'] : null),
            array('id' => 'password', 'name' => 'password', 'CNname' => '密碼：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Password', "Value" => (is_object($Data)) ? $Data['password'] : null),
            array('id' => 'password_confirmation', 'name' => 'password_confirmation', 'CNname' => '密碼確認：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Password', "Value" => (is_object($Data)) ? $Data['password'] : null),
            array('id' => 'email', 'name' => 'email', 'CNname' => '電子信箱：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['email'] : null),
            array('id' => 'role_id', 'name' => 'role_id', 'CNname' => '權限：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => ArrayToLaravelFormSelectOption($Table2), 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['role_id'] : null),
           );
        return $BodyArray;
    }
}