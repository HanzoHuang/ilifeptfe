<?php

namespace App\Http\Controllers\admin\Management\role;

use App\Http\Controllers\BackController;
use DB;
use App\Http\Requests\RoleRequest;
class BackRoleController extends BackController
{
    //配合route resource
    protected $RouteName="BackRole";
    //檔案路徑
    protected $Path="";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Role";
    //主要table
    protected $Table1="roles";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '角色名稱', 'width' => '9'),
            array('title' => '建立時間', 'width' => '9'),
            array('title' => '修改時間', 'width' => '9'),
            array('title' => '隱藏時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)->select('id','name','created_at','updated_at','deleted_at')->get();
        //DataTable中的功能按鈕
        // $Data=getDataTableImages($Data,'Product');
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "角色創建";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    public function store(RoleRequest $request)
    {   

        //use model
        $ModelName=$this->ModelName;
        $data=$ModelName::where('name',$request->name)->get();
        //使用輸入的名稱撈取資料庫資料 若有資料判定為資料重複
        if (count($data)>=1) {
           return redirect()->to($this->RouteName.'/create')->with('status','角色名稱已註冊過');
        }
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        SearchData($request->all(),$data->id,$this->RouteName);
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::withTrashed()->find($id));
        //表格標題
        $LayoutTitle = "角色修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function update(RoleRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //以輸入的名稱撈取資料
        $data=$ModelName::where('name',$request->name)->first();
        //查詢資料庫是否有同樣名稱的資料
        if (count($data)>=1) {
        //撈取的資料id與當次修改id不同 判定為不同筆資料 名稱重複
            if ($data->id!=$id) {
               return redirect()->to($this->RouteName.'/'.$id.'/edit')->with('status','角色名稱重複');
            }
        }
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        SearchData($request->all(),$id,$this->RouteName);
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        
        $BodyArray = array(
             array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
             array('id' => 'name', 'name' => 'name', 'CNname' => '角色名稱：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : null),
            array('id' => 'description', 'name' => 'description', 'CNname' => '介紹：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['description'] : null),
        );
        return $BodyArray;
    }
}
