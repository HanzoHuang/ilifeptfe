<?php

namespace App\Http\Controllers\admin\Management\role;

use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\RoleControlsRequest;
use App\RoleControls;
use DB;
use Input;
use Log;

class BackRoleControlsController extends BackController{

    //配合route resource
    protected $RouteName="BackRoleControls";
    //檔案路徑
    protected $Path="Upload/RoleControls/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\RoleControls";

    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '註冊功能', 'width' => '9'),
            array('title' => '角色', 'width' => '9'),
            array('title' => '下架時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table('role_controls')
                ->join('program_registration', 'program_registration.id', '=', 'role_controls.program_registration_id')
                ->join('roles', 'role_controls.role_id', '=', 'roles.id')
                ->select('role_controls.id','program_registration.name as program_registration_name','roles.name as roles_name','role_controls.deleted_at')
                ->get();
        // writings::all();
        //命名DataTable的ID，預設是tableWithSearch
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'AssignNewProduct' => 0,'AssignHotProduct'=>0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
        public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "新增角色權限";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleControlsRequest $request)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $ModelName::create(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($ModelName::find($id));
        //表格標題
        $LayoutTitle = "修改角色權限";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleControlsRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        $program_registration=DB::table('program_registration')->select('id','name')->get();
        $roles=DB::table('roles')->select('id','name')->get();
        $Competence=array(0=>"Y",1=>"N");                               
        $BodyArray = array(
            0 => array('id' => 'role_id', 'name' => 'role_id', 'CNname' => '角色：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => ArrayToLaravelFormSelectOption($roles), 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['role_id'] : '321'),
            1 => array('id' => 'program_registration_id', 'name' => 'program_registration_id', 'CNname' => '註冊功能：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => ArrayToLaravelFormSelectOption($program_registration), 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['program_registration_id'] : '321'),
            2 => array('id' => 'create', 'name' => 'create', 'CNname' => '新增：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $Competence, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['create'] : '321'),
            3 => array('id' => 'read', 'name' => 'read', 'CNname' => '讀取：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $Competence, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['read'] : '321'),
            4 => array('id' => 'update', 'name' => 'update', 'CNname' => '更新：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $Competence, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['update'] : '321'),
            5 => array('id' => 'delete', 'name' => 'delete', 'CNname' => '刪除：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $Competence, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['delete'] : '321'),
            6 => array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ?  $Data['id']  : null)
            );
        return $BodyArray;
    }
}
