<?php

namespace App\Http\Controllers\admin\Order;

use App\Http\Controllers\BackController;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\MenuClassRequest;
class BackOrderController extends BackController
{
     //配合route resource
    protected $RouteName="BackOrder";
    //檔案路徑
    protected $Path="Upload/Order/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\Order\order";
    //主要table
    protected $Table1="order";
    public function index()
    {
       $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '購買時間', 'width' => '5'),
            array('title' => '金額', 'width' => '5'),
            array('title' => '運費', 'width' => '5'),
            array('title' => '購買人', 'width' => '9'),
            array('title' => '電話', 'width' => '9'),
            array('title' => '地址', 'width' => '9'),
            array('title' => '收件人', 'width' => '9'),
            array('title' => '收件人電話', 'width' => '9'),
            array('title' => '狀態', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select('id','created_at','total','freight','name','tel','add','recipient_name','recipient_tel','status')
        ->get();
        foreach($Data as $Datas){
            switch ($Datas->status) {
                case '100':$Datas->status='未付款'; break;
                case '200':$Datas->status='已付款'; break;
                case '300':$Datas->status='已出貨'; break;
                default: break;
            }
        }
        //DataTable中的功能按鈕
        // $Data=getDataTableImages($Data,'Product');
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 0, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'OrderOption'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'Add'=>1,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function show($id)
    {
        $order=db::table('order')->find($id);
        switch ($order->status) {
            case '100': $order->status='未付款'; break;
            case '200': $order->status='已付款'; break;
            case '300': $order->status='已出貨'; break;            
            default:break;
        }
        switch ($order->delivery_method) {
            case '1': $order->delivery_method='貨到付款'; break;
            case '2': $order->delivery_method='線上刷卡'; break;
            case '3': $order->delivery_method='已出貨'; break;            
            default:break;
        }
        switch ($order->invoice_method) {
            case '1': $order->invoice_method='二聯式發票'; break;
            case '2': $order->invoice_method='三聯式發票,'.'<br>公司抬頭:'.$order->company.'<br>統一編號:'.$order->uniform_numbers; break;
            default:break;
        }
        $order_detail=db::table('order_detail')
            ->select(
                'order_detail.id',
                'product.zh_name as name',
                'order_detail.quantity',
                'product_size.name as size',
                'product.price',
                'product.img1'
            )
            ->join('order','order_detail.order_id','=','order.id')
            ->join('product','order_detail.product_id','=','product.id')
            ->join('product_size','product.product_size','=','product_size.id')
            ->where('order_detail.order_id',$id)
            ->get();
        return view('admin.Form.order_detail',['order'=>$order,'order_detail'=>$order_detail,'id'=>$id]);
    }
    
    public function edit($id)
    {   
       $ModelName=$this->ModelName;
       $data=$ModelName::find($id);
       $ModelName::where('id',$id)->update(['status'=>$data->status+100]);
       return redirect()->back();
    }

}
