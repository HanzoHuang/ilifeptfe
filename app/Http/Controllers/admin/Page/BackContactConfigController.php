<?php

namespace App\Http\Controllers\admin\Page;

use App\Http\Controllers\BackController;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\MenuClassRequest;
class BackContactConfigController extends BackController
{
     //配合route resource
    protected $RouteName="BackContactConfig";
    //檔案路徑
    protected $Path="Upload/ContactConfig/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\Page\ContactConfig";
    //主要table
    protected $Table1="contact_config";
    public function index($id=1)
    {
       //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "關於我們修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH','',$id);
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "新增關於我們";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    public function store(Request $request)
    {   

        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "關於我們修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH','',$id);
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function update(Request $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        // $type=$this->type;
        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            // array('id' => 'type', 'name' => 'type', 'CNname' => '類型：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $type, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['type'] : null),
            // array('id' => 'sort', 'name' => 'sort', 'CNname' => '排序：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Number', "Value" => (is_object($Data)) ? $Data['sort'] : 1,'min'=>1),
            array('id' => 'zh_address', 'name' => 'zh_address', 'CNname' => '地址(繁)：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['zh_address'] : null),
            array('id' => 'en_address', 'name' => 'en_address', 'CNname' => '地址(en)：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['en_address'] : null),
            array('id' => 'tel', 'name' => 'tel', 'CNname' => '電話：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['tel'] : null),
            array('id' => 'fax', 'name' => 'fax', 'CNname' => '傳真：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['fax'] : null),
            array('id' => 'fb_link', 'name' => 'fb_link', 'CNname' => 'FB網址：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['fb_link'] : null),
            

            // array('id' => 'about_img', 'name' => 'about_img', 'CNname' => '關於我們圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['about_img'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_about_content', 'name' => 'zh_about_content', 'CNname' => '關於我們內容(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_about_content'] : null),
            // array('id' => 'en_about_content', 'name' => 'en_about_content', 'CNname' => '關於我們內容(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content1'] : null),

            // array('id' => 'introduction_img1', 'name' => 'introduction_img1', 'CNname' => '介紹1圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['introduction_img1'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_introduction_content1', 'name' => 'zh_introduction_content1', 'CNname' => '介紹1(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_introduction_content1'] : null),
            // array('id' => 'en_introduction_content1', 'name' => 'en_introduction_content1', 'CNname' => '介紹1(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content1'] : null),

            // array('id' => 'introduction_img2', 'name' => 'introduction_img2', 'CNname' => '介紹2圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['introduction_img2'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_introduction_content2', 'name' => 'zh_introduction_content2', 'CNname' => '介紹2(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_introduction_content2'] : null),
            // array('id' => 'en_introduction_content2', 'name' => 'en_introduction_content2', 'CNname' => '介紹2(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content2'] : null),
        );
        return $BodyArray;
    }
}
