<?php

namespace App\Http\Controllers\admin\Page;

use App\Http\Controllers\BackController;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\MenuClassRequest;
class BackNewsController extends BackController
{
     //配合route resource
    protected $RouteName="BackNews";
    //檔案路徑
    protected $Path="Upload/News/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\Page\News";
    //主要table
    protected $Table1="news";
    protected $type=array(1=>'最新消息',2=>'展場資訊');
    public function index()
    {
       $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '日期', 'width' => '9'),
            array('title' => '類型', 'width' => '9'),
            array('title' => '排序', 'width' => '9'),
            array('title' => '標題(繁)', 'width' => '9'),
            array('title' => '標題(en)', 'width' => '9'),
            array('title' => '隱藏時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select('id','date','type','sort','zh_title','en_title','deleted_at')
        ->get();
        foreach($Data as $val){
            $val->type=$this->type[$val->type];
        }
        //DataTable中的功能按鈕
        // $Data=getDataTableImages($Data,'Product');
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "新增最新消息&展場資訊";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    public function store(Request $request)
    {   

        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "最新消息&展場資訊修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function update(Request $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        $type=$this->type;
        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'type', 'name' => 'type', 'CNname' => '類型：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $type, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['type'] : null),
            array('id' => 'sort', 'name' => 'sort', 'CNname' => '排序：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Number', "Value" => (is_object($Data)) ? $Data['sort'] : 1,'min'=>1),
            array('id' => 'date', 'name' => 'date', 'CNname' => '日期：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['date'] : date('Y-m-d')),
            array('id' => 'zh_title', 'name' => 'zh_title', 'CNname' => '標題1(繁)：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['zh_title'] : null),
            array('id' => 'en_title', 'name' => 'en_title', 'CNname' => '標題1(en)：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['en_title'] : null),
             array('id' => 'zh_content', 'name' => 'zh_content', 'CNname' => '內容(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_content'] : null),
             array('id' => 'en_content', 'name' => 'en_content', 'CNname' => '內容(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_content'] : null),

            array('id' => 'img', 'name' => 'img', 'CNname' => '圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_about_content', 'name' => 'zh_about_content', 'CNname' => '關於我們內容(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_about_content'] : null),
            // array('id' => 'en_about_content', 'name' => 'en_about_content', 'CNname' => '關於我們內容(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content1'] : null),

            // array('id' => 'introduction_img1', 'name' => 'introduction_img1', 'CNname' => '介紹1圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['introduction_img1'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_introduction_content1', 'name' => 'zh_introduction_content1', 'CNname' => '介紹1(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_introduction_content1'] : null),
            // array('id' => 'en_introduction_content1', 'name' => 'en_introduction_content1', 'CNname' => '介紹1(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content1'] : null),

            // array('id' => 'introduction_img2', 'name' => 'introduction_img2', 'CNname' => '介紹2圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['introduction_img2'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_introduction_content2', 'name' => 'zh_introduction_content2', 'CNname' => '介紹2(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_introduction_content2'] : null),
            // array('id' => 'en_introduction_content2', 'name' => 'en_introduction_content2', 'CNname' => '介紹2(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content2'] : null),
        );
        return $BodyArray;
    }
}
