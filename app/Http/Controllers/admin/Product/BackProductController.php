<?php

namespace App\Http\Controllers\admin\Product;

use App\Http\Controllers\BackController;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\MenuClassRequest;
class BackProductController extends BackController
{
     //配合route resource
    protected $RouteName="BackProduct";
    //檔案路徑
    protected $Path="Upload/Product/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\Product\Product";
    //主要table
    protected $Table1="product";
    protected $Table2="product_size";
    public function index()
    {
       $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '排序', 'width' => '9'),
            array('title' => '商品圖片', 'width' => '9'),
            array('title' => '商品名稱(繁)', 'width' => '9'),
            array('title' => '價格', 'width' => '9'),
            array('title' => '尺寸', 'width' => '9'),
            array('title' => '隱藏時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select(
            $this->Table1.'.id',
            $this->Table1.'.sort',
            $this->Table1.'.img1',
            $this->Table1.'.zh_name',
            $this->Table1.'.price',
            $this->Table2.'.name',
            $this->Table1.'.deleted_at'
        )
        ->join($this->Table2,$this->Table1.'.product_size','=',$this->Table2.'.id')
        ->get();
        
        //DataTable中的功能按鈕
        $Data=getDataTableImages($Data,'Upload/Product');
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "新增商品";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    public function store(Request $request)
    {   

        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "商品修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function update(Request $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        $size=ArrayToOption(db::Table('product_size')->select('id','name')->get());
        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'product_size', 'name' => 'product_size', 'CNname' => '尺寸：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $size, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['product_size'] : null),
            array('id' => 'sort', 'name' => 'sort', 'CNname' => '排序：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Number', "Value" => (is_object($Data)) ? $Data['sort'] : 1,'min'=>1),
            array('id' => 'price', 'name' => 'price', 'CNname' => '價格：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Number', "Value" => (is_object($Data)) ? $Data['price'] : 1,'min'=>1),
            array('id' => 'zh_name', 'name' => 'zh_name', 'CNname' => '商品名稱(繁)：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>'' , 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['zh_name'] : null),
            array('id' => 'en_name', 'name' => 'en_name', 'CNname' => '商品名稱(en)：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['en_name'] : null),
             array('id' => 'zh_content', 'name' => 'zh_content', 'CNname' => '商品介紹(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_content'] : null),
             array('id' => 'en_content', 'name' => 'en_content', 'CNname' => '商品介紹(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_content'] : null),
            array('id' => 'img1', 'name' => 'img1', 'CNname' => '代表圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img1'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            array('id' => 'img2', 'name' => 'img2', 'CNname' => '介紹圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img2'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            array('id' => 'img3', 'name' => 'img3', 'CNname' => '介紹圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img3'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            array('id' => 'img4', 'name' => 'img4', 'CNname' => '介紹圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img4'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            array('id' => 'img5', 'name' => 'img5', 'CNname' => '介紹圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img5'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            array('id' => 'img6', 'name' => 'img6', 'CNname' => '介紹圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['img6'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_about_content', 'name' => 'zh_about_content', 'CNname' => '商品內容(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_about_content'] : null),
            // array('id' => 'en_about_content', 'name' => 'en_about_content', 'CNname' => '商品內容(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content1'] : null),

            // array('id' => 'introduction_img1', 'name' => 'introduction_img1', 'CNname' => '介紹1圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['introduction_img1'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_introduction_content1', 'name' => 'zh_introduction_content1', 'CNname' => '介紹1(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_introduction_content1'] : null),
            // array('id' => 'en_introduction_content1', 'name' => 'en_introduction_content1', 'CNname' => '介紹1(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content1'] : null),

            // array('id' => 'introduction_img2', 'name' => 'introduction_img2', 'CNname' => '介紹2圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'FileBox2', "Value" => (is_object($Data)) ? $Data['introduction_img2'] : null,'table'=>$this->Table1,'Path'=>$this->Path),
            // array('id' => 'zh_introduction_content2', 'name' => 'zh_introduction_content2', 'CNname' => '介紹2(繁)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['zh_introduction_content2'] : null),
            // array('id' => 'en_introduction_content2', 'name' => 'en_introduction_content2', 'CNname' => '介紹2(en)：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['en_introduction_content2'] : null),
        );
        return $BodyArray;
    }
}
