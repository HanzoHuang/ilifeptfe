<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Order\Order;
use Auth;
use Redirect;
use Session;
use DB;
use Input;


class RemoveFileController extends Controller
{
    public function remove_file()
    {
        $table=Input::get('table');
        $name=Input::get('name');
        $id=Input::get('id');
        $path=Input::get('path');
        $data=db::table($table)->find($id);
        $update=array($name=>'');
        
        if($data){
           if(is_file($path.$data->$name)){
            unlink($path.$data->$name);
            db::table($table)->where('id',$id)->update($update);
            } 
        }
    return redirect()->back();
    }

}
