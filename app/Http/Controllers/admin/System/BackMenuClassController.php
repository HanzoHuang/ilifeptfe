<?php

namespace App\Http\Controllers\admin\System;

use App\Http\Controllers\BackController;
use DB;
use App\Http\Requests\MenuClassRequest;
class BackMenuClassController extends BackController
{
     //配合route resource
    protected $RouteName="BackMenuClass";
    //檔案路徑
    protected $Path="";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\MenuClass";
    //主要table
    protected $Table1="menu_class";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '角色名稱', 'width' => '9'),
            array('title' => '排序', 'width' => '9'),
            array('title' => '建立時間', 'width' => '9'),
            array('title' => '修改時間', 'width' => '9'),
            array('title' => '隱藏時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)->select('id','name','sort','created_at','updated_at','deleted_at')->orderBy('sort','asc')->get();
        //DataTable中的功能按鈕
        // $Data=getDataTableImages($Data,'Product');
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "新增選單";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    public function store(MenuClassRequest $request)
    {   

        //use model
        $ModelName=$this->ModelName;
        $data=$ModelName::where('name',$request->name)->get();
        if (count($data)>=1) {
               return redirect()->to($this->RouteName.'/create')->with('status','選單名稱重複');
        }
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "選單修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function update(MenuClassRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //以輸入的名稱撈取資料
        $data=$ModelName::where('name',$request->name)->first();
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        //全站搜尋
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        
        $BodyArray = array(
             array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
             array('id' => 'name', 'name' => 'name', 'CNname' => '選單名稱：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : null),
             array('id' => 'font_awesome', 'name' => 'font_awesome', 'CNname' => '圖示名稱：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['font_awesome'] : null),
             array('id' => 'sort', 'name' => 'sort', 'CNname' => '排序：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['sort'] : null),
        );
        return $BodyArray;
    }
    // public function index()
    // {
    //     $ListArray = array(
    //         0 => array('id' => 'name', 'width' => '300', 'aligh' => 'center', 'CNname' => '名稱', 'hidden' => '', 'sortable' => ''),
    //         1 => array('id' => 'font_awesome', 'width' => '100', 'aligh' => 'center', 'CNname' => '圖形文字', 'hidden' => '', 'sortable' => ''),
    //         2 => array('id' => 'smart_sort', 'width' => '300', 'aligh' => 'center', 'CNname' => '智能排序', 'hidden' => '', 'sortable' => ''),
    //         3 => array('id' => 'created_at', 'width' => '150', 'aligh' => 'center', 'CNname' => '建立時間', 'hidden' => '', 'sortable' => ''),
    //         4 => array('id' => 'updated_at', 'width' => '150', 'aligh' => 'center', 'CNname' => '最後更新時間', 'hidden' => '', 'sortable' => ''),
    //         5 => array('id' => 'deleted_at', 'width' => '300', 'aligh' => 'center', 'CNname' => '隱藏時間', 'hidden' => '', 'sortable' => ''),

    //     );
    //     //前台要顯示的輸入欄位陣列(ID名稱,英文名稱,中文名稱,預設內容,是否開啟防呆,額外設定,如果需要SELECT 或是其他INPUT)
    //     $BodyArray = array(
    //         0 => array('id' => 'name', 'name' => 'name', 'CNname' => '名稱：', 'data-options' => "prompt:'Please enter the name...'", 'class' => "easyui-textbox", "required" => "required=ture", "Option" => "", "type" => ""),
    //         1 => array('id' => 'font_awesome', 'name' => 'font_awesome', 'CNname' => '名稱：', 'data-options' => "prompt:'Please enter the font_awesome...'", 'class' => "easyui-textbox", "required" => "required=ture", "Option" => "", "type" => ""),

    //     );
    //     //名稱
    //     $ModelName = 'MenuClass';
    //     //開始排序
    //     $SmartSort      = "sort";
    //     $ControllerName = "BackMenuClass";
    //     //標題
    //     $title = "後臺選單分類";
    //     //副標題
    //     $BodyTitle = "請輸入相關內容";
    //     //內容說明
    //     $IntroductionBody = "歡迎使用本系統";
    //     //關鍵欄位
    //     $Key = "name";
    //     //以下呼叫EasyUI建立
    //     //////////////////////////////////////////////////////////
    //     $EasyuiTtile = EasyuiTtile($title);
    //     //最上面的功能啟用(css,高度,寬度,標題,要顯示內容,功能設定,指定聯結)
    //     $EasyuiTopBar = EasyuiTopBar("easyui-datagrid", "1400px", "600px", $title, $ListArray, "singleSelect='true' pagination='true'", $ModelName, $ControllerName, $SmartSort);
    //     //是否開啟功能(新增、修改、刪除   0關閉 1打開)
    //     $EasyuiFunctionBar = EasyuiFunctionBar(array("insert" => 1, "edit" => 1, "delete" => 1, "disabled" => 1), $ModelName, $Key);
    //     //內容陣列
    //     $EasyuiBody = EasyuiBody($BodyTitle, $BodyArray);
    //     //介紹的div，css可以自己修改
    //     $EasyuiIntroduction = EasyuiIntroduction($IntroductionBody, "float:left;padding: 100px 10px;");
    //     ///////////////////////////////////////////////////////////

    //     $IntroductionBody = "歡迎使用本系統,請設定平台帳號導向";
    //     return view('admin.EasyUI.index', ['EasyuiTtile' => $EasyuiTtile, 'EasyuiTopBar' => $EasyuiTopBar, 'EasyuiIntroduction' => $EasyuiIntroduction, 'EasyuiFunctionBar' => $EasyuiFunctionBar, 'EasyuiBody' => $EasyuiBody, 'BodyTitle' => $EasyuiIntroduction, 'LevelOne' => '系統設定', 'LevelTwo' => 'Menu管理']);
    // }
}
