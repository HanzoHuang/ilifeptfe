<?php
namespace App\Http\Middleware;
// First copy this file into your middleware directoy
use Closure;
use Gate;
use Route;
class Acl {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$RouteName = Route::currentRouteName();
		if (Gate::denies('ACL', $RouteName)) {
			$ProgramName = explode(".", $RouteName);
			return redirect()->route($ProgramName[0].'.index')->with('status', '您沒有此項目'.$ProgramName[1].'權限，請通知管理員');
		}else{
			return $next($request);
		}

	}
}
?>