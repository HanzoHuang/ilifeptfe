<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;
use DB;

class FooterData {
	protected $session;

	public function __construct(Store $session) {
		$this->session = $session;
	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$foot_data = DB::select("select cs_tel,cs_mail,line,business_hour from footer where id=1");
		$this->session->put('FooterData', $foot_data[0]);

		return $next($request);
	}

}