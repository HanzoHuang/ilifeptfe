<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MemberUpdateRequest   extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'name' => 'required',
	        'tel' => 'required',
	        'email' => 'required',
	        'password' => 'required_with:old_password',
	        'password' => 'same:ck_password',
	        'ck_password' => 'required_with:old_password',
	    ];
    }	
    public function messages()
	{
	    return [
	        'name.required' => '請填姓名',
	        'tel.required' => '請填電話',
	        'email.required' => '請填電話',
	        'password.required_with'=>'請填寫欲修改的新密碼',
	        'password.same'=>'與確認新密碼不符',
	        'ck_password.required_with'=>'請填確認新密碼',
	        'application_date.required' => '請填申請日期',
	        'applicant.required' => '請填申請人',
	        'applicant_number.required' => '請填申請人身分證',
	        'household_registration.required' => '請填申請人戶籍',
	        'tel.required' => '請填申請人電話',
	        'phone.required' => '請填申請人行動電話',
	        'relationship.required' => '請填關係',
	        'address.required' => '請填地址',
	        'name.required' => '請填逝者姓名',
	        'name_number.required' => '請填逝者身分證',
	        'name_household_registration.required' => '請填逝者戶籍',
	        'gender.required' => '3',
	        'identity.required' => '4',
	        'discount.required' => '5',
	        'dead_date.required' => '請填逝者死亡日期',
	        'dead_reason.required' => '請填逝者死亡原因',
	        'dead_location.required' => '請填逝者死亡地點',
	        'birthday.required' => '請填逝者生日',
	        'expected_date.required' => '請填預計進塔日期',
	        'cabinet_type.required' => '請填預計進塔日期',
	        'cabinet_class_position.required' => '6',
	        // 'cabinet_class_floor.required' => '7',
	        'cabinet_class_aera.required' => '8',
	        'cabinet_class_row.required' => '9',
	        'cabinet_code.required' => '請搜尋空位並選擇',
	        'client.required' => '請填委託人姓名',
	        'client_number.required' => '請填委託人身分證',
	        'client_household_registration.required' => '請填委託人戶籍',
	        'client_tel.required' => '請填委託人電話',
	        'client_phone.required' => '請填委託人行動電話',
	        'client_address.required' => '請填委託人行動地址',
	        'file1.required' => '請上傳申請人身分證',
	        'file1.sometimes' => '申請人身分證必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	        'file2.required' => '請上傳火化許可證',
	        'file2.sometimes' => '火化許可證必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	        'file3.required' => '請上傳死亡證明書',
	        'file3.sometimes' => '死亡證明書必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	        'file4.required' => '請上傳戶籍謄本',
	        'file4.sometimes' => '戶籍謄本必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	        'file5.required' => '請上傳亡者除戶謄本',
	        'file5.sometimes' => '亡者除戶謄本必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	    ];
	}
//          'file1' => 'sometimes|mimes:jpeg,bmp,png,gif,jpg|max:1024',
//	        'img.image'		 =>'驗證欄位檔案必須為圖片格式（ jpeg、png、bmp、gif、 或 svg ）',


}
