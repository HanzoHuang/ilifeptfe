<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class RoleControlsRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		$data=Input::all();
	    return [
	    	'role_id' => 'unique:role_controls,role_id,NULL,id,program_registration_id,'.$data['program_registration_id']
	    ];
    }	
    public function messages()
	{
	    return [
	        'role_id.unique'  => '這個角色已經註冊過此功能',
	    ];
	}


}
