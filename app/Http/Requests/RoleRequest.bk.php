<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class RoleRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		$data=Input::all();
	    return [
	    	'name' => 'required'
	    ];
    }	
    public function messages()
	{
	    return [
	        'role_name.required'  => '這個角色名稱不得重複',
	    ];
	}


}
