<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
Route::get('/', function () {
    return Redirect::to('Home');
});
Route::resource('Home', 'Reception\ReceptionController@index');
Route::resource('BackLogin', 'admin\BackLoginController@index');

//vvvvvvvvvvvvvvvvv auth link vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
Route::group(['middleware' => ['auth', 'sessiontimeout']], function () {
    Route::get('BackHome', ['as' => 'BackHome.index', 'uses' => 'admin\BackLoginController@Home',function(){
        return view('BackHome');
    }]);
    Route::group(['middleware' => ['acl']], function () {
        //^^^^^^^^^^^^^^^^基礎資料
        //log管理
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        //服務公告管理
        Route::resource('BackAnnouncementInformation', 'admin\PublicInformation\BackAnnouncementInformationController');
        //公告分類管理
        Route::resource('BackAnnouncementInformationClass', 'admin\PublicInformation\BackAnnouncementInformationClassController');
        //活動花絮
        Route::resource('BackBanner', 'admin\PublicInformation\BackBannerController');
        //塔位查詢
        Route::resource('BackTowerSelect', 'admin\TowerSelect\BackTowerSelectController');
        //各類櫃位及牌位使用情形及數量統計  
        Route::resource('BackPaper', 'admin\Paper\BackPaperController');
        //各項收入統計
        Route::resource('BackPaperMoneySum', 'admin\Paper\BackPaperMoneySumController');
        //各項新增收入及退費金額統計
        Route::resource('BackPaperMoneyDetail', 'admin\Paper\BackPaperMoneyDetailSumController');
        //牌位查詢
        Route::resource('BackTombstoneSelect', 'admin\Select\BackTombstoneSelectController');
        Route::get('BackTombstoneSelectChangeSelect', ['as' => 'BackTombstone.ChangeSelect', 'uses' => 'admin\Select\BackTombstoneSelectController@change_select']);

        //櫃位查詢
        Route::resource('BackCabinet', 'admin\Select\BackCabinetController');
        //貴為查詢條件變動
        Route::get('BackCabinetSelect', ['as' => 'BackCabinet.ChangeSelect', 'uses' => 'admin\Select\BackCabinetController@change_select']);

        //圖片刪除
        Route::get('BackRemove', ['as' => 'BackRemove.remove', 'uses' => 'admin\RemoveFileController@remove_file']);

        //帳號管理
        Route::resource('BackUserManagement', 'admin\Management\BackUserManagementController');
        //role管理
        Route::resource('BackRole', 'admin\management\role\BackRoleController'); 
        //程式註冊
        Route::resource('BackRoleControls', 'admin\Management\role\BackRoleControlsController'); 
        //角色維護
        Route::resource('BackProgramRegistration', 'admin\System\BackProgramRegistrationController');
        //選單註冊
        Route::resource('BackMenuClass', 'admin\System\BackMenuClassController');
        //聯絡我們Banner
        Route::resource('BackContactBanner', 'admin\Page\BackContactBannerController');
        //首頁設定
        Route::resource('BackIndexConfig', 'admin\Page\BackIndexConfigController');
        //關於我們Banner
        Route::resource('BackAboutBanner', 'admin\Page\BackAboutBannerController');
        Route::resource('BackAbout', 'admin\Page\BackAboutController');
        Route::resource('BackPtfe', 'admin\Page\BackPtfeController');
        Route::resource('BackQaBanner', 'admin\Page\BackQaBannerController');
        Route::resource('BackQa', 'admin\Page\BackQaController');
        Route::resource('BackContactConfig', 'admin\Page\BackContactConfigController');
        Route::resource('BackNews', 'admin\Page\BackNewsController');

        Route::resource('BackProduct', 'admin\Product\BackProductController');
        Route::resource('BackProductSize', 'admin\Product\BackProductSizeController');
        Route::resource('BackOrder', 'admin\Order\BackOrderController');

        Route::resource('OrientationData', 'admin\Page\OrientationDataController');

        Route::get('OrientationData', ['as' => 'Admin.OrientationData', 'uses' => 'admin\Orientation\BackOrientationController@change_data']);
        Route::group(['namespace' => 'admin'], function () {
            Route::get('BackReturnList/ReturnCheck', ['as' => 'ReturnList.update', 'uses' => 'ReturnList\BackReturnListController@return_checkon']);
            Route::get('BackReturnList/CancelReturn', ['as' => 'ReturnList.update', 'uses' => 'ReturnList\BackReturnListController@cancel_return']);
        });

    });
});
//^^^^^^^^^^^^^^^^^ auth link ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Route::group(['prefix' => 'auth'], function () {
    //註冊
    Route::get('/register', ['as' => 'register.index', 'uses' => 'Auth\AuthController@getregister']);
    Route::post('/register', ['as' => 'register.process', 'uses' => 'Auth\AuthController@Postregister']);
});
//登入
Route::post('Login', ['as' => 'Auth.authenticate', 'uses' => 'Auth\AuthController@authenticate']);
//登出
Route::get('Logout', ['as' => 'BackLogin.Logout', 'uses' => 'admin\BackLoginController@Logout']);

//前台登入
Route::post('RLogin', ['as' => 'Reception.RLogin', 'uses' => 'Reception\ReceptionController@RLogin']);
//前台登出
Route::get('RLogout', ['as' => 'Reception.RLogout', 'uses' => 'Reception\ReceptionController@RLogout']);

Route::get('About', ['as' => 'Reception.About', 'uses' => 'Reception\ReceptionController@About']);
Route::get('News', ['as' => 'Reception.News', 'uses' => 'Reception\ReceptionController@News']);
Route::get('NewsDetail/{id?}', ['as' => 'Reception.NewsDetail', 'uses' => 'Reception\ReceptionController@NewsDetail']);
Route::get('Ptfe', ['as' => 'Reception.Ptfe', 'uses' => 'Reception\ReceptionController@Ptfe']);
Route::get('PtfeDetail/{id?}', ['as' => 'Reception.PtfeDetail', 'uses' => 'Reception\ReceptionController@PtfeDetail']);
Route::get('Product', ['as' => 'Reception.Product', 'uses' => 'Reception\ReceptionController@Product']);
Route::get('ProductList', ['as' => 'Reception.ProductList', 'uses' => 'Reception\ReceptionController@ProductList']);
Route::get('ProductDetail/{id}', ['as' => 'Reception.ProductDetail', 'uses' => 'Reception\ReceptionController@ProductDetail']);
Route::get('QA', ['as' => 'Reception.QA', 'uses' => 'Reception\ReceptionController@QA']);
Route::get('Contacts', ['as' => 'Reception.Contacts', 'uses' => 'Reception\ReceptionController@Contacts']);
Route::get('Checkout', ['as' => 'Reception.Checkout', 'uses' => 'Reception\ReceptionController@Checkout']);

Route::post('Done', ['as' => 'Reception.Done', 'uses' => 'Reception\ReceptionController@Done']);

Route::get('Forgot', ['as' => 'Reception.Forgot', 'uses' => 'Reception\ReceptionController@Forgot']);
Route::get('LoginPage', ['as' => 'Reception.LoginPage', 'uses' => 'Reception\ReceptionController@LoginPage']);
Route::get('Member', ['as' => 'Reception.Member', 'uses' => 'Reception\ReceptionController@Member']);
Route::post('MemberDataUpdate', ['as' => 'Reception.MemberDataUpdate', 'uses' => 'Reception\ReceptionController@MemberDataUpdate']);
Route::get('OrderDetail/{id}', ['as' => 'Reception.OrderDetail', 'uses' => 'Reception\ReceptionController@OrderDetail']);
Route::get('Registar', ['as' => 'Reception.Registar', 'uses' => 'Reception\ReceptionController@Registar']);
Route::get('ChangeLang', ['as' => 'Reception.ChangeLang', 'uses' => 'Reception\ReceptionController@ChangeLang']);
Route::post('Registar', ['as' => 'Reception.RegistarPost', 'uses' => 'Reception\ReceptionController@RegistarPost']);
Route::get('Cart', ['as' => 'Reception.Cart', 'uses' => 'Reception\ReceptionController@Cart']);
Route::get('AddCart', ['as' => 'Reception.AddCart', 'uses' => 'Reception\CartController@AddCart']);
Route::get('DelCart', ['as' => 'Reception.DelCart', 'uses' => 'Reception\CartController@DelCart']);
Route::get('EditQuantityCart', ['as' => 'Reception.EditQuantity', 'uses' => 'Reception\CartController@EditQuantity']);
Route::get('OAuthLogin/{type}', ['as' => 'Auth.oauthLogin', 'uses' => 'Auth\AuthController@oauthLogin']);
Route::get('OAuthCallback/{type}', ['as' => 'Auth.oauthCallback', 'uses' => 'Auth\AuthController@oauthCallback']);

