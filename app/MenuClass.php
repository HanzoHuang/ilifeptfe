<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuClass extends Model {
	use SoftDeletes;
	protected $table = 'menu_class';
	protected $fillable = ['name', 'sort','font_awesome'];
	protected $dates = ['deleted_at'];
	public function ProgramRegistration() {
		return $this->hasOne('App\ProgramRegistration', 'foreign_key');
	}
}
?>