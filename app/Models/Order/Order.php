<?php

namespace App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {
	use SoftDeletes;
	protected $table = 'order';
	protected $fillable  = ['status','no','user_id','total','freight','delivery_method','county','district','zipcode','add','name','email','tel','recipient_name','recipient_email','recipient_tel','invoice_method','company','uniform_numbers','ext'];

}
