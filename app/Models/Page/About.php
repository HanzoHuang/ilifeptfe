<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class About extends Model {
	use SoftDeletes;
	protected $table = 'about';
	protected $fillable  = ['type','zh_content','en_content','icon','zh_title','en_title','sort'];

}
