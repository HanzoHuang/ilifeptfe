<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AboutBanner extends Model {
	use SoftDeletes;
	protected $table = 'about_banner';
	protected $fillable  = ['img'];

}
