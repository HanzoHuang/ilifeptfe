<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactBanner extends Model {
	use SoftDeletes;
	protected $table = 'contact_banner';
	protected $fillable  = ['img'];

}
