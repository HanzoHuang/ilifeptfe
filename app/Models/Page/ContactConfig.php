<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactConfig extends Model {
	use SoftDeletes;
	protected $table = 'contact_config';
	protected $fillable  = ['zh_address','en_address','tel','fax','fb_link'];

}
