<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IndexConfig extends Model {
	use SoftDeletes;
	protected $table = 'index_config';
	protected $fillable  = ['banner_img','about_img','zh_about_content','introduction_img1','zh_introduction_content1','introduction_img2','zh_introduction_content2','en_about_content','en_introduction_content1','en_introduction_content2'];

}
