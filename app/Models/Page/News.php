<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {
	use SoftDeletes;
	protected $table = 'news';
	protected $fillable  = ['type','date','img','zh_title','en_title','zh_content','en_content','sort'];

}
