<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ptfe extends Model {
	use SoftDeletes;
	protected $table = 'ptfe';
	protected $fillable  = ['zh_title1','en_title1','zh_title2','en_title2','zh_content','en_content','sort','img'];

}
