<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Qa extends Model {
	use SoftDeletes;
	protected $table = 'qa';
	protected $fillable  = ['zh_answer','en_answer','zh_question','en_question','sort'];

}
