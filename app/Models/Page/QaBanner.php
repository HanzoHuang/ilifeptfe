<?php

namespace App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QaBanner extends Model {
	use SoftDeletes;
	protected $table = 'qa_banner';
	protected $fillable  = ['img'];

}
