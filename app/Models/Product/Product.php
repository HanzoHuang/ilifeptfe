<?php

namespace App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {
	use SoftDeletes;
	protected $table = 'product';
	protected $fillable  = ['product_size','zh_name','en_name','zh_content','en_content','img1','img2','img3','img4','img5','img6','price','sort'];

}
