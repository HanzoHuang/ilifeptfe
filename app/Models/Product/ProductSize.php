<?php

namespace App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSize extends Model {
	use SoftDeletes;
	protected $table = 'product_size';
	protected $fillable  = ['name'];

}
