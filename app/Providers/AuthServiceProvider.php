<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider {
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		'App\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	 * Register any application authentication / authorization services.
	 *
	 * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
	 * @return void
	 */
	public function boot(GateContract $gate) {
		parent::registerPolicies($gate);
		$gate->define('ACL', function ($user, $RouteName) {
			if ($RouteName != null) {
				$ProgramName = explode(".", $RouteName);
				$ProgramRegistration = \App\ProgramRegistration::where('program_name', $ProgramName[0])->get();
				if ($ProgramRegistration->count() == 0) {
					return true;
				}
				$Data = \App\RoleControls::where('role_id', $user->role_id)->where('program_registration_id', $ProgramRegistration[0]->id)->first();
				if ($Data != '[]') {
					switch (getCurrentMethodName()) {
						case 'index':
							if($Data->read!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'edit':
							if($Data->update!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'update':
							if($Data->update!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'store':
							if($Data->create!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'add':
							if($Data->create!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'delete':
							if($Data->delete!=0){
								return false;
							}else{
								return true;
							}
							break;
						default:
							return true;
							break;
					}
				} else {
					return false;
				}
			} else {
				return true;
			}
		});
		$gate->define('ACL_view', function ($user, $RouteName,$CRUD) {
			if ($RouteName != null) {
				$ProgramName = explode(".", $RouteName);
				$ProgramRegistration = \App\ProgramRegistration::where('program_name', $ProgramName[0])->get();
				$Data = \App\roleControls::where('role_id', $user->role_id)->where('program_registration_id', $ProgramRegistration[0]->id)->first();
				if ($Data != '[]') {
					switch ($CRUD) {
						case 'read':
							if($Data->read!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'update':
							if($Data->update!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'create':
							if($Data->create!=0){
								return false;
							}else{
								return true;
							}
							break;
						case 'delete':
							if($Data->delete!=0){
								return false;
							}else{
								return true;
							}
							break;
						default:
							return true;
							break;
					}
				} else {
					return false;
				}
			} else {
				return true;
			}
		});
	}
}
