<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleControls extends Model {
	use SoftDeletes;
	protected $table = 'role_controls';
	protected $fillable = ['program_registration_id', 'sort', 'role_id', 'create', 'read', 'delete', 'update'];
	protected $dates = ['deleted_at'];
	public function ProgramRegistration() {
		return $this->belongsTo('App\ProgramRegistration');
	}
	public function Role() {
		return $this->belongsTo('App\Role');
	}
}
?>