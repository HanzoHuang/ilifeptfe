<?php
function EasyuiTtile($Title)
{
    $data = "<div style='float:left'>";
    $data .= "<div id='page-heading'>";
    $data .= "<h1>" . $Title . "</h1>";
    $data .= "</div>";

    return $data;
}
function EasyuiTopBar($Class = "easyui-datagrid", $Width, $Height, $Title, $ListArray, $Option = "singleSelect='true' pagination='true", $ModelName, $ControllerName = "", $SpecifyTheSort = 1, $JoinKey = array(0 => 1, 1 => 1, 2 => 1))
{
    $data = "<table rownumbers='false'  id='myDG' method='get' class='" . $Class . "' style='width:" . $Width . ";height:" . $Height . "'";
    $data .= "url='./EasyUI/Select?ModelName=" . $ModelName . "&ControllerName=" . $ControllerName . "&SpecifyTheSort=" . $SpecifyTheSort . "&JoinKey=" . $JoinKey[0] . "&JoinStr=" . $JoinKey[1] . "&JoinStr2=" . $JoinKey[2] . "' toolbar='#toolbar'";
    $data .= "title='輸入相關資料'";
    $data .= $Option;
    $data .= ">";
    $data .= "<thead>";
    $data .= "<tr>";
    foreach ($ListArray as $key => $value) {
        //$data .= "<th field='" . $ListArray[$key]["id"] . "'sortable='" . $ListArray[$key]["sortable"] . "' " . $ListArray[$key]["hidden"] . " width='" . $ListArray[$key]["width"] . "'  align='" . $ListArray[$key]["aligh"] . "'>" . $ListArray[$key]["CNname"] . "</th>";
        $data .= "<th field='" . $ListArray[$key]["id"] . "'sortable='" . $ListArray[$key]["sortable"] . "' " . $ListArray[$key]["hidden"] . " width='" . $ListArray[$key]["width"] . "'  align='" . $ListArray[$key]["aligh"] . "'>" . $ListArray[$key]["CNname"] . "</th>";
    }
    $data .= "</tr>";
    $data .= "</thead>";
    $data .= "</table>";

    return $data;
}
function EasyuiFunctionBar($OptionList, $ModelName, $Key, $IdKey = 1)
{
    $data = "<div id='toolbar'>";
    if ($OptionList["insert"] == 1) {$data .= '<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="NewData' . "('EasyUI','" . csrf_token() . "','" . $ModelName . "','" . $Key . "','" . $IdKey . "')" . '">新增資料</a>';}
    if ($OptionList["edit"] == 1) {$data .= '<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="EditData' . "('EasyUI/update','" . csrf_token() . "','" . $ModelName . "','" . $Key . "','" . $IdKey . "')" . '">更改資料</a>';}
    if ($OptionList["delete"] == 1) {$data .= '<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="RemoveData' . "('EasyUI/delete','" . csrf_token() . "','" . $ModelName . "','" . $Key . "','" . $IdKey . "')" . '">刪除資料</a>';}
    if ($OptionList["disabled"] == 1) {$data .= '<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="DisabledData' . "('EasyUI/disabled','" . csrf_token() . "','" . $ModelName . "','" . $Key . "','" . $IdKey . "')" . '">顯示/隱藏</a>';}
    $data .= "<div>";

    return $data;
}
function EasyuiBody($BotyTiltl, $BodyList)
{
    $data = '<div id="dlg" class="easyui-dialog" style="width:500px;height:450px;padding:10px" closed="true" buttons="#dlg-buttons">';
    $data .= '<div class="ftitle">';
    $data .= $BotyTiltl;
    $data .= '</div><br>';
    $data .= '<form id="fm"  method="post"  enctype="multipart/form-data">';
    foreach ($BodyList as $key => $value) {
        $data .= '<div class="fitem">';
        $data .= "<label style='width:150PX'>";
        $data .= $BodyList[$key]['CNname'];
        $data .= "</label>";
        $data .= EasyUIBodyCase($BodyList[$key]['type'], $BodyList[$key]['class'], $BodyList[$key]['name'], $BodyList[$key]['id'], $BodyList[$key]['Option'], $BodyList[$key]['data-options'], $BodyList[$key]['required']);
        $data .= '</div>';
    }
    $data .= '<input id="id" name="id"  type="hidden">';
    $data .= '<input type="hidden" id="_token" name="_token"  />';
    $data .= '<input type="hidden" id="ModelName" name="ModelName"  />';
    $data .= '<input type="hidden" id="Key" name="Key"  />';
    $data .= '<input type="hidden" id="IdKey" name="IdKey"  />';
    $data .= '</form>';
    $data .= '</div>';
    $data .= '<div id="dlg-buttons">';
    $data .= '<a href="#" class="easyui-linkbutton" iconCls="icon-ok"onclick="Save()">存檔</a>';
    $data .= "<a href='#' class='easyui-linkbutton' iconCls='icon-cancel'";
    $data .= 'onclick="javascript:';
    $data .= "$('#dlg').dialog('close')";
    $data .= '">取消</a>';
    $data .= "</div>";
    $data .= "</div></div></div>";

    return $data;
}
function EasyUIBodyCase($Type, $Class, $Name, $Id, $Option, $DataOptions, $Required)
{
    $Data = "";
    switch ($Type) {
        case 'select':
            $Data .= '<select  class="' . $Class . '" name="' . $Name . '" id="' . $Id . '">';
            $Data .= $Option;
            $Data .= '</select>';
            break;
        default:
            $Data .= '<input data-options="' . $DataOptions . '"  ' . $Required . ' class="' . $Class . '" name="' . $Name . '" id="' . $Id . '">';
            break;
    }
    return $Data;
}
function EasyuiIntroduction($Body, $CSS = "float:left;padding: 100px 10px;")
{
    $BodyArray = explode(",", $Body);
    $data      = '<div style="' . $CSS . '">';
    foreach ($BodyArray as $key => $value) {
        $data .= "<li>";
        $data .= $BodyArray[$key];
        $data .= "</li>";
    }
    $data .= '</div>';

    return $data;
}
function ArrayToLaravelFormSelectOption($Data)
{
    $array = array();
    foreach ($Data as $key => $value) {
         $array = array_add($array, $Data[$key]->id, $Data[$key]->name);
    }
    return $array;
}
function ArrayToOption($Data)
{
    $array = array();
    foreach ($Data as $key => $value) {
         $array = array_add($array, $Data[$key]->id, $Data[$key]->name);
    }
    return $array;
}
function ArrayToSelectOption($Data)
{
    $Option ="";
    foreach ($Data as $key => $value) {
        $Option .= "<option value=" . $Data[$key]->id . ">" . $Data[$key]->name . "</option>";
    }
    return $Option;
}
function DataTableJs($Id = "tableWithSearch", $BodyArray, $ModelData, $Option = 1, $ControllerName = 1, $Print = 1, $Hand = 1)
{
    ini_set('memory_limit', '256M');
    $NewModelData = "";
    $Data         = "";
    $Data .= '<table class="table table-striped table-hover dt-responsive" id="' . $Id . '">';
    $Data .= "</table>";
    foreach ($ModelData as $key => $value) {
        $Str = "";
        foreach ($value as $key2 => $value2) {
            $Str .= "'" . str_replace("\r\n","",str_replace("'","’",$value->$key2)) . "',";

        }
        if ($Option != 1) {
            $Str .= DataTableJsOption($Option, $value, $ControllerName);
        }

        $NewModelData .= '[' . $Str . '],';
    }
    //下面開始是js
    $Data .= "";
    $Data .= '<script>';
    $Data .= '$(document).ready(function(){';
    $Data .= 'var opt={';
    if(strpos(getCurrentControllerName(),'BackTombstoneSelect')!==false){
       $Data .= '"scrollX":true,';
    }
    $Data .= '"bAutoWidth": true,';
    $Data .= '"aaSorting":[],';
    $Data .= '"aoColumns":[';
    foreach ($BodyArray as $key => $value) {
        //,"bVisible": false,"bSearchable": false
        $Data .= '{"sTitle":"' . $BodyArray[$key]['title'] . '"},';
    }
    $Data .= '],"aaData":[' . $NewModelData . '],';
    if ($Print != 1) {
        $Data .= "dom: 'Bfrtip',";
        $Data .= "buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],";
    }
    if ($Hand != 1) {
        $Data .= "searching: false,";
        $Data .= "paging: false,";
        $Data .= "bInfo : false";
    }
    $Data .= '};';
    $Data .= '$("#' . $Id . '").dataTable(opt)';
    $Data .= '});';
    $Data .= '</script>';
    return $Data;
}

function FormTtile($LayoutTitle, $Title, $Body)
{
    $Data = "";
    $Data .= '<div class="container-fluid container-fixed-lg">';
    $Data .= '  <div class="panel panel-transparent">';

    $Data .= '<div class="panel-body">';
    $Data .= '<div class="row">';
    $Data .= '<div class="col-sm-10">';
    $Data .= "<h3>" . $Title . "</h3>";
    $Data .= "<p>";
    $Data .= $Body;
    $Data .= "</p>";
    $Data .= "<br>";

    return $Data;
}

function FormBody($BodyList, $Url, $Status = "",$method,$ModelName=0,$id=0)
{
    $Data = "";
    $Data .= Form::open(array('url' => $Url, 'files' => 'true', 'class' => 'form-horizontal', 'files' => true,'method'=>$method));
    //echo print_r($BodyList);exit;
    foreach ($BodyList as $key => $value) {
        if($value["Type"] == 'Hidden'){
            $Data .= ByCase($value,$ModelName,$id);
        }else{
            $Data .= '<div class="form-group">';
            $Data .= '<label for="fname" class="col-sm-3 control-label">';
            $Data .= $BodyList[$key]['CNname'];
            $Data .= "</label>";
            $Data .= '<div class="col-sm-9">';
            $Data .= ByCase($value,$ModelName,$id);
            $Data .= '</div>';
            $Data .= '</div>';
        }
        
    }
    $Data .= '<div class="row">';
    $Data .= '<div class="col-sm-3">';
    $Data .= '<p>';
    $Data .= '送出前，請詳細檢查是否有遺漏的資料';
    $Data .= '</p>';
    $Data .= '</div>';
    $Data .= '<div class="col-sm-9">';
    $Data .= FormBodyStatus($Status);
    $Data .= '</button>';
    $Data .= '</div>';
    $Data .= '</div>';
    $Data .= Form::close();
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";

    return $Data;
}

function ByCase($Value,$ModelName='',$id='')
{
    $Data = "";
    switch ($Value["Type"]) {
        case 'Email':
            $Data .= Form::email($Value['name'], $Value["Value"], ['class' => $Value['class'], 'placeholder' => $Value['placeholder'], 'required' => $Value['Option']]);
            break;
        case 'Select':
            if ($Value['Option'] == null) {
                $Data .= Form::select($Value['name'], array('' => ''), 'default', array('id' => $Value["id"], 'class' => $Value['class'],'readonly'=>$Value['readonly']));
            } else {
                $Data .= Form::select($Value['name'], $Value['Option'], $Value["Value"], array('id' => $Value["id"], 'class' => $Value['class'],$Value['readonly']));
            }
            break;
        case 'Select2':
            if ($Value['Option'] == null) {
                $Data .= Form::select($Value['name'], array('' => ''), 'default', array('id' => $Value["id"], 'class' => $Value['class'],$Value['readonly']));
            } else {
                $Data .= Form::select($Value['name'], $Value['Option'], $Value["Value"], array('id' => $Value["id"], 'class' => $Value['class'],$Value['readonly']));
            }
            $Data.='<script>';
            $Data.='$(document).ready(function() { $("#'.$Value["id"].'").select2();dropdownAutoWidth:true });';
            $Data.='</script>';
            break;
        case 'Number':
            $Data .= Form::input('number', $Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'],'max'=>(isset($Value['max']))? $Value['max']:null,'min'=>(isset($Value['min']))? $Value['min']:null  ));
            //$Data .= '<input value="' . $Value['Value'] . '"' . $Value['Option'] . ' type="text" class="' . $Value['class'] . '" id="' . $Value['id'] . '" placeholder="' . $Value['placeholder'] . '" " name="' . $Value['name'] . '">';
            break;
        case 'Text':
            $Data .= Form::text($Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'], $Value['Option'], 'id' => $Value['id']));
            //$Data .= '<input value="' . $Value['Value'] . '"' . $Value['Option'] . ' type="text" class="' . $Value['class'] . '" id="' . $Value['id'] . '" placeholder="' . $Value['placeholder'] . '" " name="' . $Value['name'] . '">';
            break;
        case 'Color':
            $Data .= Form::input('color',$Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'], $Value['Option'], 'id' => $Value['id']));
            //$Data .= '<input value="' . $Value['Value'] . '"' . $Value['Option'] . ' type="text" class="' . $Value['class'] . '" id="' . $Value['id'] . '" placeholder="' . $Value['placeholder'] . '" " name="' . $Value['name'] . '">';
            break;
        case 'Password':
            $Data .= Form::password($Value['name'], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'], $Value['Option'], 'id' => $Value['id']));
            break;
        case 'Textarea':
            $Data .= Form::textarea($Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'],'cols'=>20));
            break;
        case 'Hidden':
            $Data .= Form::hidden($Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            break;
        case 'checkbox':
            $Str = "0" . $Value['Value'];
            foreach ($Value['Option'] as $key => $value) {
                $Data .= Form::checkbox($Value['name'], $Value['Option'][$key], (strpos($Str, $Value['Option'][$key]) > 0) ? true : false);
            }
            break;
        case 'radio':
            foreach($Value['Option'] as $option_key => $option_value){
                $Data .= Form::Label($option_value);
                if($Value['Value']==$option_key){
                    $Data .= Form::Radio($Value['name'],$option_key,true);
                }else{
                    $Data .= Form::Radio($Value['name'],$option_key);
                }
            }
            break;
        case 'HR':
            $Data .='<hr style="color:#000">';
            break;
        case 'FileBox':
            $Data .= '<div class="fallback">';
            $Data .= Form::file($Value['name']);
            $Data .= '</div>';
            $Str="";
            if(!empty($Value['Value']) && $Value['Option']!="file"){
                if(!is_file($Value['Path'] . $Value['Value'])){
                    $Data .=  getImagerLightbox('images/no_image_thumb.gif',300);

                }else{
                    $Data .=  getImagerLightbox($Value['Path'] . $Value['Value'],300);

                }
                $Str="請注意只能上傳'jpg'、'jpeg'、'png'格式，大小限制1MB";

            }elseif(!empty($Value['Value']) & $Value['Option']=="file"){
                $Data .=  '<a class="btn" href="'.asset($Value['Path'].$Value['Value']).'">'.$Value['Value'].'</a>';
                $Str="";
            }elseif($Value['Option']!="file"){
                $Data .=  getImagerLightbox('images/no_image_thumb.gif',300);
            }
            $remark = '';
            if($Value['placeholder'] != ''){
               $remark= "<font color='red'>備註：".$Value['placeholder']."</font>";
            }
            $Data .= "<div style='margin-top: 2px;'>".$remark.$Str."</div>";
            if(isset($Value['table'])){
                if ($id!=0&&!empty($Value['Value'])) {
                $Data .= "<div style='margin-top: 2px;'><a class='removefile' href='".route('Back.RemoveFile')."?table=".$Value['table']."&name=".$Value['name']."&id=".$id."&path=".$Value['Path']."'>刪除檔案</a></div>";
                }
            }
            break;
        case 'InputDate':
            $Data .= '<div id="datepicker-component" class="input-group date col-sm-8">';
            $Data .= Form::text($Value['name'], $Value["Value"], array('id' => $Value['id'], 'class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            $Data .= '<span class="input-group-addon">';
            $Data .= '<i class="fa fa-calendar"></i></span>';
            $Data .= '</div>';
            $Data .='<script>';
            $Data .='$(function() {';
            $Data .='$.fn.datepicker.defaults.format = "yyyy-mm-dd";';
            $Data .='$("#'.$Value['id'].'").datepicker();';
            $Data .='});';
            $Data .='</script>';
            break;
        case 'InputTime':
            $Data .= '<div id="timepicker-component" class="input-group time col-sm-8">';
            $Data .= Form::text($Value['name'], $Value["Value"], array('id' => $Value['id'], 'class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            $Data .= '</div>';
            $Data .='<script>';
            $Data .='$(function() {';
            $Data .='$("#'.$Value["id"].'").timepicker();';
            $Data .='});';
            $Data .='</script>';
            break;
        case 'InputDateTime':
            $Data .= '<div id="input-append date" class="input-group time col-sm-8"  data-date="12-02-2012" data-date-format="dd-mm-yyyy">';
            $Data .= Form::text($Value['name'], $Value["Value"], array('id' => $Value['id'], 'class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            $Data .= '<span class="add-on"><i class="icon-remove"></i></span>';
            $Data .= '<span class="add-on"><i class="icon-th"></i></span>';
            $Data .= '</div>';
            $Data .='<script>';
            $Data .='$(function() {';
            $Data .='$("#'.$Value["id"].'").datetimepicker({"setStartDate":"'.date("Y-m-d").'",autoclose:true,minuteStep:15});';
            $Data .='});';
            $Data .='</script>';
            break;
        case 'Label':
            $Data .= '<Label id="'.$Value['id'].'" name="'.$Value['name'].'" class="'.$Value['class'].'">';
            $Data .= $Value['Value'];
            $Data .= '</Label>';
            break;
        case 'EmptyRow':
            $Data .= '<div id="'.$Value['id'].'" name="'.$Value['name'].'" class="input-group date col-sm-8"></div>';
            break;
        case 'FileBox2':
            $Data .= '<div class="fallback">';
            $Data .= Form::file($Value['name']);
            $Data .= '</div>';
            $Str="";
            if(!empty($Value['Value']) & $Value['Option']!="url"){
                if(!is_file($Value['Path'] . $Value['Value'])){
                    $Data .=  getImagerLightbox('images/no_image_thumb.gif',300);
                }else{
                    $Data .=  getImagerLightbox($Value['Path'] . $Value['Value'],300);
                    $Data.='<br><a class="btn" href="'.Route('BackRemove.remove').'?id='.$id.''.'&name='.$Value['name'].'&table='.$Value['table'].'&path='.$Value['Path'].'">刪除檔案</a>';
                }
                $Str="請注意只能上傳'jpg'、'jpeg'、'png'格式，大小限制1MB";
            }elseif(!empty($Value['Value']) & $Value['Option']=="url"){
                $Data .=  '<a href="'.$Value['Path']. $Value['Value'].'">請下載</a>';
                $Str="";
            }else{
                $Data .=  getImagerLightbox('images/no_image_thumb.gif',300);
            }
            $remark = '';
            if($Value['placeholder'] != ''){

               $remark= "<font color='red'>備註：".$Value['placeholder']."</font>";
            }
            $Data .= "<div style='margin-top: 2px;'><br><br>".$remark.$Str."</div>";
            break;
    }
    return $Data;
}
//DataTableOption中的Check判斷
function DataTableCheck($value, $ControllerName)
{
    $Str = "";
    switch ($value->status) {
        case '處理中':
            $Str .= '<a href="' . $ControllerName . '/ReturnCheck?id=' . $value->id . '" class="btn btn-danger" style="background-color:#18a689;border:none">可退貨</a>';
            $Str .= '<a href="' . $ControllerName . '/CancelReturn?id=' . $value->id . '" class="btn btn-danger" style="background-color:#c75757;border:none">取消退貨</a>';
            break;
        case '退款中':
            $Str .= '<a href="' . $ControllerName . '/ReturnCheck?id=' . $value->id . '" class="btn btn-danger" style="background-color:#f0ad4e";border:none>已退款</a>';
            $Str .= '<a href="' . $ControllerName . '/CancelReturn?id=' . $value->id . '" class="btn btn-danger" style="background-color:#c75757;border:none">取消退貨</a>';
            break;
        case '已退款':
            break; 
        case 5:
            break;    
    }
    return $Str;
}
function FormBodyStatus($Status)
{
    $Data = '';
    switch ($Status) {
        case '0':
            $Data .= '<button class="btn btn-success" type="submit" disabled>';
            $Data .= '送審中';
            break;
        case '1':
            $Data .= '<button class="btn btn-success" type="submit">';
            $Data .= '修改';
            break;
        case '2':
            $Data .= '<button class="btn btn-success" type="submit" disabled>';
            $Data .= '審核完畢';
            break;
        case '3':
            $Data .= '<button class="btn btn-success" type="submit" disabled>';
            $Data .= '禁止修改';
            break;
        default:
            $Data .= '<button class="btn btn-success" type="submit">';
            $Data .= '送出';
            break;
    }
    return $Data;
}
function DataTableJsOption($Option, $data, $ControllerName)
{
    $Str = "'";
    foreach ($Option as $key => $value) {
        if ($Option[$key] != 1) {
            continue;
        }
        switch ($key) {
            case 'Img':
                break;
            case 'Add':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"create"])) {
                    $Str .= '<a href="' . $ControllerName . 'Add?id=' . $data->id . '"class="btn btn-tag">新增</a>';
                }
                break;
            case 'Edit':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    if(!empty($data->order_id)){
                        if(db::table('order')->where('id',$data->order_id)->first()->status==1){
                            if(Auth::user()->role_id!=3){
                                $Str .= '<a href="#"class="btn btn-info" title="訂單已確認" style="margin-right: 10px;">訂單已確認-無法修改</a>';
                            }else{
                                $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '/edit "class="btn btn-info" title="修改" style="margin-right: 10px;"><i class="fa fa-pencil"></i></a>';
                            }
                        }else{
                        $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '/edit "class="btn btn-info" title="修改" style="margin-right: 10px;"><i class="fa fa-pencil"></i></a>';
                        }
                    }else{
                        if(isset($data->trip_id)){
                            $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '/edit?trip_id='.$data->trip_id.'"class="btn btn-info" title="修改" style="margin-right: 10px;"><i class="fa fa-pencil"></i></a>'; 

                        }else{
                            $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '/edit "class="btn btn-info" title="修改" style="margin-right: 10px;"><i class="fa fa-pencil"></i></a>'; 

                        }
                    }
                }
                break;
            case 'Read':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset($ControllerName . 'Read/' . $data->id) . '"class="btn btn-primary" title="詳細內容" style="margin-right: 10px;"><i class="fa fa-align-justify"></i></a>';
                }
                break;
            case 'Read2':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset($ControllerName . 'Read2/' . $data->id) . '"class="btn btn-primary" title="機票日期" style="margin-right: 10px;"><i class="fa fa-cloud-upload" aria-hidden="true"></i></a>';
                }
                break;
                case 'Read3':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset($ControllerName . 'Read3/' . $data->id) . '"class="btn btn-primary" title="機票" style="margin-right: 10px;">機票</a>';
                }
                break;
                case 'Read4':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {

                    $Str .= '<a href="' . asset('BackOrderRead4/'.$data->id) . '"class="btn btn-warning" title="飯店項目" style="margin-right: 10px;background-color:red"><i class="fa fa-tag"></i></a>';
                }
                break;
                case 'Read5':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset('BackOrderRead5/' . $data->id) . '"class="btn btn-primary" title="交通項目" style="margin-right: 10px;"><i class="fa fa-code-fork"></i></a>';
                }
                break;
                case 'Read6':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset('BackOrderRead6/' . $data->id) . '"class="btn btn-primary" title="活動項目" style="margin-right: 10px;"><i class="fa fa-th"></i></a>';
                }
                break;
            case 'ReadDetail':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    
                    $Str .= '<a href="' . $ControllerName . 'ReadDetail?id=' . $data->id . '"class="btn btn-primary" title="詳細內容" style="margin-right: 10px;"><i class="fa fa-align-justify"></i></a>';
                    
                }
                break;
            case 'ChangeStatus':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    if($data->status=='關閉'){
                        $text='開啟';
                        $Str .= '<a href="' . $ControllerName . 'CahngeStatus?id=' . $data->id . '"class="btn" style="background-color:green;color:#fff" title="" style="margin-right: 10px;">'.$text.'</a>';
                    }else{
                        $text='關閉';
                        $Str .= '<a href="' . $ControllerName . 'CahngeStatus?id=' . $data->id . '"class="btn" Style="background-color:red;color:#fff" title="" style="margin-right: 10px;">'.$text.'</a>';
                    }
                    
                }
                break;
            case 'Excel':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    
                    $Str .= '<a href="BackOrderExcel/' . $data->id . '"class="btn btn-primary" title="下載EXCEL" style="margin-right: 10px;"><i class="fa fa-sort-amount-asc"></i></a>';
                    
                }
                break;
            case 'Delete':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"delete"])) {
                    if(!empty($data->order_id)){
                        if(db::table('order')->where('id',$data->order_id)->first()->status==1){
                        }else{
                        $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '" class="btn btn-success ask-delete jquery-postback"  data-method="DELETE"  title="刪除" style="margin-right: 10px;"><i class="fa fa-trash-o"></i></a>';
                        }
                    }else{
                        $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '" class="btn btn-success ask-delete jquery-postback"  data-method="DELETE"  title="刪除" style="margin-right: 10px;"><i class="fa fa-trash-o"></i></a>';
                    }
                }
                break;
            case 'Claim':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    if(empty($data->u_name)){
                   $Str .= '<a href="Claim/' . $data->id . '" class="btn btn-success" title="業務員認領" style="margin-right: 10px;"><i class="fa fa-book" aria-hidden="true"></i></a>';
                    }
                }
                break;
            case 'SmartSort':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    $Str .= '<a href="' . $ControllerName . '?Type=1&Id=' . $data->id . '&Sort=' . $data->sort . '" class="btn btn-tag"><img style="width:15px" src=images/list_up.png></a>';
                    $Str .= '<a href="' . $ControllerName . 'Select?Type=2&Id=' . $data->id . '&Sort=' . $data->sort . '" class="btn btn-tag"><img style="width:15px" src=images/list_down.png></a>';
                }
                break;
            case 'AssignNewProduct':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    $Str .= '<button class="btn btn-complete active" title="指定為新品" style="margin:5px;" onclick="'.$ControllerName.'Customize1('.$data->id.');"><i class="fs pg-bag"></i></button>';
                }
                break;
            case 'AssignHotProduct':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    $Str .= '<button class="btn btn-danger active" title="指定為熱銷品" style="margin:5px;" onclick="'.$ControllerName.'Customize2('.$data->id.');"><i class="fs fa fa-tag"></i></button>';
                }    
                break;
            case 'Detail':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . $ControllerName . 'ItemDetail?id=' . $data->id . '" class="btn btn-success" title="商品名細" style="margin-right: 10px;"><i class="fa fa-table"></i></a>';
                }
                break;
            case 'Shelf':
                $Str .= '<a href="BackWareHouseShelf?wh_id=' . $data->id . '" class="btn btn-success" title="儲位編輯" style="margin-right: 10px;"><i class="fa fa-table"></i></a>';
                break;
            case 'RecvBody':
                $Str .= '<a href="BackRecvBody?form_id=' . $data->id . '" class="btn btn-success" title="進貨商品" style="margin-right: 10px;"><i class="fa fa-table"></i></a>';
                break;
            case 'Down':
                $Str .= '<a href="' . $ControllerName . 'Down?id=' . $data->id . '" class="btn btn-success" title="下載原始檔案" style="margin-right: 10px;"><i class="fa fa-cloud-download"></i></a>';
                break;
            case 'Excel':
                $Str .= '<a href="' . $ControllerName . 'Excel?id=' . $data->id . '" class="btn btn-info" title="Excel" style="margin-right: 10px;"><i class="fa fa-file-excel-o"></i></a>';
                break;
            case 'customer_service':
                $Str .= '<a href="'.asset('BackOrderCS').'/' . $data->id . '" class="btn btn-complete" title="詳細資料" style="margin-right: 10px;"><i class="fs-14 fa fa-align-justify"></i></a>';
                break;
            case 'TouristDetail':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackOrderDetail/' . $data->id . '" class="btn btn-success" title="團員名冊" style="margin-right: 10px;"><i class="fa fa-book"></i></a>';
                }
                break;
            case 'ActivityDetail':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackOrderActivityDetail?id=' . $data->id . '" class="btn btn-warning" title="活動項目" style="margin-right: 10px;"><i class="fa fa-th"></i></a>';
                }
                break;
            case 'TrafficDetail':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackOrderTrafficDetail?id=' . $data->id . '" class="btn btn-warning" title="交通項目" style="margin-right: 10px;"><i class="fa fa-code-fork"></i></a>';
                }
                break;
            case 'HotelDetail':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackOrderHotelDetail?id=' . $data->id . '" class="btn btn-warning" title="飯店項目" style="margin-right: 10px;"><i class="fa fa-tag"></i></a>';
                }
                break;
            case 'QUOConfirm':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackQUO_Order?id=' . $data->id . '" class="btn btn-info" title="轉成訂單" style="margin-right: 10px;"><i class="fa fa-money"></i></a>';
                }
                break;
            case 'CashFlow':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackOrderCashFlow?id=' . $data->id . '" class="btn btn-danger" title="收支表" style="margin-right: 10px;"><i class="fa fa-bank"></i></a>';
                }
                break;
            case 'IncomeReport':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackQUO_Order?id=' . $data->id . '" class="btn btn-primary" title="成本分析表" style="margin-right: 10px;"><i class="fa fa-pie-chart"></i></a>';
                }
                break;
            case 'ReturnBtn':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="BackReturnOrder/'.$data->id . '" class="btn btn-primary" title="退回需求單" style="margin-right: 10px;"><i class="fa fa-reply"></i></a>';
                }
                break;
            case 'SetTripCopy':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    
                    $Str .= '<a href="' . asset($ControllerName . 'Copy/' . $data->id) . '"class="btn btn-primary" title="複製行程" style="margin-right: 10px;"><i class="fa fa-files-o"></i></a>';
                }
                break;
            case 'CopyTour':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    
                    $Str .= '<a href="' . asset($ControllerName . 'Copy/' . $data->id) . '"class="btn btn-primary" title="複製行程" style="margin-right: 10px;"><i class="fa fa-files-o"></i></a>';
                }
                break;
            case 'FreeExerciseCopy':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . $ControllerName . 'Copy/' . $data->id . '"class="btn btn-primary" title="複製行程" style="margin-right: 10px;"><i class="fa fa-files-o"></i></a>';
                }
                break;
            case 'SetTrip':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset( 'BackSetTrip/' . $data->id) . '"class="btn btn-primary" title="套裝行程" style="margin-right: 10px;">套裝行程</a>';
                }
                break;
            case 'FreeTrip':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset( 'BackFreeExercise/' . $data->id) . '"class="btn btn-primary" title="自由行行程" style="margin-right: 10px;">自由行行程</a>';
                }
                break;
            case 'Hotel':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset( 'BackHotel/' . $data->id) . '"class="btn btn-primary" title="飯店" style="margin-right: 10px;">飯店</a>';
                }
                break;
            case 'Traffic':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {

                    $Str .= '<a href="' . asset('BackTrafficServices/' . $data->id) . '"class="btn btn-primary" title="當地交通" style="margin-right: 10px;">當地交通</a>';
                }
                break;
            case 'Activity':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {

                    $Str .= '<a href="' . asset('BackActivity/' . $data->id) . '"class="btn btn-primary" title="活動" style="margin-right: 10px;">活動</a>';
                }
                break;
            case 'Quo':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {

                    $Str .= '<a href="' . asset('BackQUO' . $data->id) . '"class="btn btn-primary" title="需求單" style="margin-right: 10px;">需求單</a>';
                }
                break;
            case 'Order':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {

                    $Str .= '<a href="' . asset('BackOrder/' . $data->id) . '"class="btn btn-primary" title="正式訂單" style="margin-right: 10px;">正式訂單</a>';
                }
                break;
            case 'SetTripTour':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset('BackSetTripTour/' . $data->id) . '"class="btn btn-warning" title="旅行團" style="margin-right: 10px;"><i class="fa fa-list-ol"></i></a>';
                }
                break;
            case 'SetTripPriceList':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset('BackSetTripPriceList/' . $data->id) . '"class="btn btn-info" title="價格表" style="margin-right: 10px;"><i class="fa fa-list-alt"></i></a>';
                }
                break;
            case 'Appliction':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    switch ($data->status) {
                        case '申請中':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="繳費" style="margin-right: 10px;">繳費</a>';
                            break;
                        case '已繳費':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="入塔" style="margin-right: 10px;">入塔</a>';
                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'ApplictionChange':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    switch ($data->status) {
                        case '異動申請中':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="繳費" style="margin-right: 10px;">繳費</a>';
                        break;
                        case '異動已繳費':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="異動" style="margin-right: 10px;">異動</a>';
                            break;
                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'Appliction':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    switch ($data->status) {
                        case '異動申請中':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="繳費" style="margin-right: 10px;">繳費</a>';
                        break;
                        case '異動已繳費':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="異動" style="margin-right: 10px;">異動</a>';
                            break;
                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'Appliction2':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    switch ($data->status) {
                        case '申請遷出':
                        $Str .= '<a href="' . asset($ControllerName.'/' . $data->id) . '/edit"class="btn btn-info" title="出塔" style="margin-right: 10px;">出塔</a>';
                        break;
                            # code...
                            break;
                    }
                }
                break;
            case 'Goto':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                        $Str .= '<a href="' . asset('BackCabinet/' . $data->no) . '"class="btn btn-info" title="明細" style="margin-right: 10px;">明細</a>';
                }
                break;
            case 'Goto2':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                        $Str .= '<a href="' . asset('BackTombstoneSelect/' . $data->no) . '"class="btn btn-info" title="明細" style="margin-right: 10px;">明細</a>';
                }
                break;
            case 'OrderOption':
                if (Gate::allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                        $Str .= '<a href="' . asset('BackOrder/'.$data->id) . '"class="btn btn-info" title="明細" style="margin-right: 10px;">明細</a>';
                        switch ($data->status) {
                            case '未付款':
                                $Str .= '<a href="' . asset('BackOrder/'.$data->id) . '/edit"class="btn btn-info" title="付款" style="margin-right: 10px;">付款</a>';
                                    # code...
                                    break;
                            case '已付款':
                                $Str .= '<a href="' . asset('BackOrder/'.$data->id) . '/edit"class="btn btn-info" title="付款" style="margin-right: 10px;">出貨</a>';
                                    # code..
                                    break;
                                
                                default:
                                    # code...
                                    break;
                        }

                }
                break;
        }
    }
    $Str .= "',";
    return $Str;
}
/**
 * 获取当前控制器名
 *
 * @return string
 */
function getCurrentControllerName()
{
    return getCurrentAction()['controller'];
}

/**
 * 获取当前方法名
 *
 * @return string
 */
function getCurrentMethodName()
{
    return getCurrentAction()['method'];
}

/**
 * 获取当前控制器与方法
 *
 * @return array
 */
function getCurrentAction()
{
    $action = \Route::current()->getActionName();
    list($class, $method) = explode('@', $action);

    return ['controller' => $class, 'method' => $method];
}

function getImagerLightbox($url,$width=200)
{
    $str='<a href="'.asset($url).'" data-lightbox="roadtrip">';
    $str.=Html::image($url,'',array('style'=>'width:'.$width.'px'));
    $str.='</a>';

    return $str;
}

// 上傳處理
function getUploadFile($path, $file, $oldFile = '')
{
    $random_sn = date('ymdHis').rand(11111, 99999); 
    if (!empty($oldFile)) {
        if (is_file($path.$oldFile)) {
            unlink($path.$oldFile); 
        }
    }
    $fileName = $random_sn.".".$file->getClientOriginalExtension();
    $file->move($path, $fileName);
    return $fileName;
}

function getDataTableImages($Data,$path)
{
    foreach($Data as $idx => $keys){
        foreach ($keys as $key => $value) {
            if ((false !== ($rst = strpos($value,'jpg')))||(false !== ($rst = strpos($value,'JPG')))||(false !== ($rst = strpos($value,'GIF')))||(false !== ($rst = strpos($value,'gif')))||(false !== ($rst = strpos($value,'jpeg')))||(false !== ($rst = strpos($value,'JPEG')))||(false !== ($rst = strpos($value,'png')))||(false !== ($rst = strpos($value,'PNG')))||(false !== ($rst = strpos($value,'bmp')))||(false !== ($rst = strpos($value,'BMP')))) {
                $filename = $Data[$idx]->$key;
                if(!is_file($path."/".$filename)){
                    $Data[$idx]->$key = '<img src="images/no_image_thumb.gif" style=" width:50px;" />';
                }else{
                    $Data[$idx]->$key = getImagerLightbox(asset($path."/".$filename),50);
                }    
            }
        }
    }
    return $Data;
}

function RequestForeach($data,$Path){
    foreach ($data as $key => $value) {
        switch (gettype($data[$key])) {
            case 'string':
                if($key=="password"){
                    $data[$key]=bcrypt($data[$key]);
                }
                if($key=="new_password"){
                    $data[$key]=bcrypt($data[$key]);
                }
                break;
            case 'object':
                $data[$key]=getUploadFile($Path,$data[$key]);
                break;
            case 'integer':
                break;
            default:
                $data[$key]=implode(",",$data[$key]);
                break;
        }
    }
    return $data;
}
function SearchRequestForeach($data){
    $Str="";
    foreach ($data as $key => $value) {
        switch (gettype($data[$key])) {
            case 'string':
               if($key!="_token" and $key!="_method" and $key!="id"){
                $Str.=$data[$key].",";
               }
                break;
            case 'object':
                break;
            default:
                # code...
                break;
        }
    }
    return substr($Str,0,-1);
}
function OrmDelete($Model){
    if ($Model->trashed()) {
        $Model->restore();
        return 0;
    }else{
        $Model=$Model->delete();
        return 1;
    }
}
function SearchData($data,$id,$RouteName){
    $Search =\App\Models\Search\Search::firstOrCreate(['search_id' => $id,'controller'=>$RouteName]);
    $Search->string=SearchRequestForeach($data);
    $Search->save();
}
function mail_action($request,$toEmail){
    foreach ($request as $key => $value) {
            $mail[$key] = $value;
            }
            $mail['toEmail'] = $toEmail;
            //去除token
            unset($mail['_token']);
            Mail::send('reception.contact_form', ['mail' => $mail], function ($message) use ($mail) {
                $message->from($mail['email'], $mail['name']);
                $message->to($mail['toEmail'])->subject($mail['name'].'寄一封信給您');
            });
}
function order_total($order_id){
    $order=db::table('order')->where('id',$id)->first();
        $start=strtotime(date($order->start_date));
        $end=strtotime(date($order->end_date));
        $days=($end-$start)/86400;
        $order_ticket=db::table('order_ticket')->where('order_id',$id)->get();
        $order_total=0;
        if (count($order_ticket)>0) {
            foreach($order_ticket as $key =>$value){
                $ticket=db::Table('ticket')->where('id',$value->ticket_id)->first();
                $order_total+=$order->adult_quantity*$ticket->adult_price+$order->child_quantity*$ticket->child_price;
            }
        }
        $order_activity=db::Table('order_activity')->where('order_id',$id)->get();
        if (count($order_activity)>0) {
            foreach($order_activity as $key =>$value){
                $activity=db::Table('activity')->where('id',$value->activity_id)->first();
                $order_total+=$value->adult_quantity*$activity->adult_price+$value->child_quantity*$activity->child_price;
            }
        }
        $order_traffic=db::Table('order_service')->where('order_id',$id)->get();
        if (count($order_traffic)>0) {
            foreach($order_traffic as $key =>$value){
                $activity=db::Table('traffic_services_read')->where('id',$value->service_id)->first();
                $order_total+=$value->adult_quantity*$activity->adult_price+$value->child_quantity*$activity->child_price;
            }
        }
        $order_hotel=db::table('order_hotel')->where('order_id',$id)->orderBy('id','desc')->take(1)->get();
        if (count($order_hotel)>0) {
            foreach($order_hotel as $key =>$value){
                $hotel=db::Table('hotel')->where('id',$value->hotel_id)->first();
                $room=db::Table('hotel_room')->where('id',$value->room_id)->first();
                $order_total+=$room->price*$days;
            }
        }
        $order_detail=db::Table('order_detail')->where('order_id',$id)->get();
        foreach($order_detail as $key=>$value){
            $order_total+=$value->insurance;
        }
        db::table('order')->where('id',$id)->update(['total',$order_total]);
}
function option()
{
    $option=['Img' => 0, 'Edit' => 0, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'AssignNewProduct' => 0,'AssignHotProduct'=>0,'Read'=>0];
    return $option;
}
function days($start,$end)
{
    // dd(strtotime($end),strtotime($start));
    return (strtotime($end)-strtotime($start))/86400;
}
function add_cost($array,$Data)
{
    if(Auth::user()->role_id==3){
        $array[]=array('id' => 'cost', 'name' => 'cost', 'CNname' => '成本：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Number', "Value" => (is_object($Data)) ? $Data['cost'] : null);
    }
    return $array;
}
function CabinetSys()
{
    $CabinetSys=array();
    $museum=array(1=>'生命紀念館');
    $floor=array(1=>'1樓');
    $class=array('A'=>'A-個人','B'=>'B-夫妻','C'=>'C-家族');
    $tombstone_class=array('A'=>'A','B'=>'B','C'=>'C');
    $position=array(1=>'座南朝北',2=>'座北朝南',3=>'座東朝西');
    $row=array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8);
    $layer=array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12);
    $gender=array(1=>'男',2=>'女');
    $type=array(0=>'本鄉鄉民',1=>'本縣縣民',2=>'外縣市民');
    $application_type=array(1=>'申請',2=>'已繳費',3=>'已入塔');
    $status=array(100=>'申請中',200=>'已繳費',300=>'已入塔',400=>'異動申請中',500=>'異動已繳費',600=>'已異動',700=>'申請遷出',800=>'已遷出',250=>'已異動於其他位置',350=>'已異動於其他位置',399=>'塔位已遷出',699=>'塔位已遷出');
    $CabinetSys['museum']=$museum;
    $CabinetSys['floor']=$floor;
    $CabinetSys['class']=$class;
    $CabinetSys['position']=$position;
    $CabinetSys['row']=$row;
    $CabinetSys['layer']=$layer;
    $CabinetSys['gender']=$gender;
    $CabinetSys['type']=$type;
    $CabinetSys['application_type']=$application_type;
    $CabinetSys['tombstone_class']=$tombstone_class;
    $CabinetSys['status']=$status;

    return $CabinetSys;
}

//code 櫃位  number 逝者身分證字號
function Exist($table,$code,$status,$number='',$new_code='')
{
    switch ($status) {
        case '100':
            db::table($table)->where('code',$code)->update(['number'=>$number]);
            break;
        case '200':
            db::table($table)->where('code',$code)->update(['number'=>$number]);
            break;
        case '300':
            db::table($table)->where('code',$code)->update(['number'=>$number]);
            break;
        case '400':
            db::table($table)->where('code',$code)->update(['number'=>$number]);
            break;
        case '500':
            db::table($table)->where('code',$code)->update(['number'=>$number]);
            break;
        case '600':
            db::table($table)->where('code',$code)->update(['number'=>null]);
            db::table($table)->where('code',$new_code)->update(['number'=>$number]);
            break;
        case '700':
            db::table($table)->where('code',$code)->update(['number'=>$number]);
            break;
        case '800':
            db::table($table)->where('code',$code)->update(['number'=>null]);
            break;
    }
    return true;
}

//類別(櫃位或牌位),樓,區域,排,方位,塔位編碼,申請號碼,申請人身分證字號,死者身分證字號,櫃位類型(長生、一般),申請時間,進塔時間,死亡時間,
function ExistLog($class_type,$class_floor,$class_area,$class_row,$class_position,$code,$no,$number,$user_number,$type,$application_date,$expected_date,$death_date,$status,$name,$user_name,$tel,$phone,$identity)
{
     db::table('pd_log')->insert([
        'class_type'=>$class_type,
        'class_floor'=>$class_floor,
        'class_area'=>$class_area,
        'class_row'=>$class_row,
        'class_position'=>$class_position,
        'code'=>$code,
        'no'=>$no,
        'number'=>$number,
        'user_number'=>$user_number,
        'status'=>$status,
        'type'=>$type,
        'application_date'=>$application_date,
        'expected_date'=>$expected_date,
        'death_date'=>$death_date,
        'user_id'=>Auth::user()->id,
        'name'=>$name,
        'user_name'=>$user_name,
        'tel'=>$tel,
        'phone'=>$phone,
        'identity'=>$identity,

    ]);
    return true;
}