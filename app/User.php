<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract {
	use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sex','name','en_name', 'email', 'password','account', 'role_id','oauth_type','oauth_id','address','tel','birthday','profession','education_level','annual_income','fax','country','identity','name','img','ext'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	protected $dates = ['deleted_at'];
	public function role() {
		return $this->hasOne('App\Role', 'id', 'role_id');
	}
	public function hasRole($roles) {
		$this->have_role = $this->getUserRole();
		// Check if the user is a root account
		if ($this->have_role->name == 'Root') {
			return true;
		}
		if (is_array($roles)) {
			foreach ($roles as $need_role) {
				if ($this->checkIfUserHasRole($need_role)) {
					return true;
				}
			}
		} else {
			return $this->checkIfUserHasRole($roles);
		}
		return false;
	}
	private function getUserRole() {
		return $this->role()->getResults();
	}
	private function checkIfUserHasRole($need_role) {
		return (strtolower($need_role) == strtolower($this->have_role->name)) ? true : false;
	}

}
