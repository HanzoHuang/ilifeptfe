<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder {
	public function run() {
		foreach (range(1, 20) as $number) {
			\App\Post::create([
				'title' => '測試假文章',
				'sub_title' => '這是副標題',
				'content' => '這是假的文章內容',
				'page_view' => 1,
			]);
		}
	}
}
