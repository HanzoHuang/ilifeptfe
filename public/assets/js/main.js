﻿$(function() {


    setTimeout(function() {
        $('.scroll_top').click(function() {
            $('html,body').animate({ scrollTop: 0 }, 500);
        });
    }, 300);

});


//fixed mega menu
$(window).scroll(function() {
    if ($(window).scrollTop() >= 121) {
        $('header').addClass('header_fixed');
    } else {
        $('header').removeClass('header_fixed');
    }
});


//Scroll Top
$(window).load(function() {
    $(window).bind('scroll resize', function() {
        var $this = $(this);
        var $this_Top = $this.scrollTop();
        if ($this_Top < 680) {
            $('.scroll_top').stop().css({ display: "none" });
        }
        if ($this_Top > 680) {
            $('.scroll_top').stop().css({ display: "block" });
        }
    }).scroll();


});
