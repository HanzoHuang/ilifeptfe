$(function() {
    $("#product_category_id").change(function() {
        $.ajax({
            url: "BackProductVariety/AjaxSelect",
            data: { id: $('#product_category_id').val(), _token: $('#_token').val() },
            type: "POST",
            context: document.body
        }).done(function(data) {
            $("#product_item_id option").remove();
            $("#product_item_id").append(data);
        });
    });
});

function NewData(Url, token, ModelName, Key, IdKey) {
    $('#dlg').dialog('open').dialog('setTitle', '編輯');
    $('#fm').form('clear');
    url = Url;
    //指定SELECT OPTION
    $("select").each(function() {
        if ($(this).find("option").length > 0) {
            $(this).find("option:first").prop("selected", true);
        }
    });
    $('#_token').val(token);
    $('#ModelName').val(ModelName);
    $('#Key').val(Key);
    $('#IdKey').val(IdKey);
}

function EditData(Url, token, ModelName, Key, IdKey) {
    $('#_token').val(token);
    $('#ModelName').val(ModelName);
    $('#password').val("");
    $('#Key').val(Key);
    $('#IdKey').val(IdKey);
    $.ajax({
        url: "BackProductVariety/AjaxSelect",
        data: { id: $('#product_category_id').val(), _token: $('#_token').val() },
        type: "POST",
        context: document.body
    }).done(function(data) {
        $("#product_item_id option").remove();
        $("#product_item_id").append(data);
    });
    var row = $('#myDG').datagrid('getSelected');
    if (row) {
        if (typeof(row.id) !== 'undefined') {
            $('#dlg').dialog('open');
            $('#fm').form('load', row);
            url = Url;
        } else {
            alert("undefined");
        }
    }
}

function Save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            //alert('sub :'+ url);
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                // close the dialog
                $('#myDG').datagrid('reload');
                // reload the user data
            } else {
                $.messager.show({
                    title: 'Error',
                    msg: result.msg
                });
            }
        }
    });
}

function RemoveData(Url, token, ModelName) {
    var row = $('#myDG').datagrid('getSelected');
    if (row) {
        $.messager.confirm('警告！', '您確定要刪除資料嗎?', function(r) {
            if (r) {
                $.post(Url, {
                    id: row.id,
                    _token: token,
                    ModelName: ModelName,
                }, 'json');
            }
            window.location.reload();
        });
    }
}

function DisabledData(Url, token, ModelName) {
    var row = $('#myDG').datagrid('getSelected');
    if (row) {
        $.messager.confirm('警告！', '您確定要改變狀態嗎?', function(r) {
            if (r) {
                $.post(Url, {
                    id: row.id,
                    _token: token,
                    ModelName: ModelName
                }, 'json');
            }
            window.location.reload();
        });
    }
}
