;
(function() {

    'use strict';
    $(function() {

        // iPad and iPod detection  
        var isiPad = function() {
            return (navigator.platform.indexOf("iPad") != -1);
        };

        var isiPhone = function() {
            return (
                (navigator.platform.indexOf("iPhone") != -1) ||
                (navigator.platform.indexOf("iPod") != -1)
            );
        };

        // Burger Menu
        var burgerMenu = function() {
            $('.menu').click(function() {
                $('.offcanvas-collapse').toggleClass('open');
                if ($('.offcanvas-collapse').hasClass('open')) {
                    $('.burger').addClass('active');
                } else {
                    $('.burger').removeClass('active');
                }
                $('body').toggleClass("no-scroll");
            });
        };

        var windowScroll = function() {
            $(window).scroll(function() {
                var header = $('#header'),
                    scroll = $(window).scrollTop();

                if (scroll >= 50) {
                    $(header).addClass('fixed');
                }
                else {
                    $(header).removeClass('fixed');
                }
            });
        };

        new WOW().init();

        var goToTop = function() {
            $('.js-gotop').on('click', function(event) {

                event.preventDefault();

                $('html, body').animate({
                    scrollTop: $('html').offset().top
                }, 500, 'easeInOutExpo');

                return false;
            });

            $(window).scroll(function() {
                var $win = $(window);
                if ($win.scrollTop() > 200) {
                    $('#side-btn').addClass('active');
                    $('#caption-de').addClass('active');
                } else {
                    $('#side-btn').removeClass('active');
                    $('#caption-de').removeClass('active');
                }
                var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
                if (scrollBottom < 1500) {
                    $('#caption-de').removeClass('active');
                }
                var $win = $(window).width();
                if ($win < 768) {
                    if (scrollBottom < 2400) {
                        $('#caption-de').removeClass('active');
                    }
                }
            });

        };
        burgerMenu();
        windowScroll();
        goToTop();


    });


}());