<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Home' => 'Home',
    'about' => 'About',
    'product' => 'Product',
    'news' => 'News',
    'contact' => 'Contact',
    'faq' => 'Faq',
    'aikengjiang'=>'Aikengjiang',
    'add'=>'1F., No.742, Ziqiang Rd., Shalu Dist., Taichung City 433, Taiwan (R.O.C.)',
    'contact_the_way'=>'Contact The Way',
    'return_product_list'=>'Return Product List',
    'more'=>'More',
    'return_news'=>'Return News',
    'contact_message'=>'If you need more information, please contact Aikengjiang',
    'contact_message2'=>'If you have any questions, please feel free to contact us',
    'company'=>'Company',
    'contact_person'=>'Contact Person',
    'tel'=>'Tel',
    'message'=>'Message',
    'Send'=>'Send',
    'more'=>'More',
    'characteristic'=>'Electrolytic water characteristics',
    'online_shopping'=>'Online Shopping',
    'online_shopping_text'=>'The most professional online shopping center, all kinds of staging zero interest rates, support SSL security transactions, support a variety of credit cards',
    'sgs'=>'SGS',
    'sgs_text'=>'All products are SGS certified quality products, you can feel at ease to use our products',
    'contact_text'=>'Have any product consultation or product purchase on the issue, please feel free to contact us, will have someone to serve you',
    'facebook_text'=>'Want to know more about the latest news and articles to share, please join our fan group, we will publish the latest articles from time to time',
];
