@extends('layouts.admin.AdminLogin')
@section('title','後台管理')
@section('content')


<div class="login-wrapper ">
			<div class="bg-pic">
				<img src="{{ URL::asset('backend/img/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg')}}" data-src="webroot/img/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="webroot/img/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">
				<div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
					<h2 class="semi-bold text-white"> Pages make it easy to enjoy what matters the most in the life</h2>
					<p class="small">
						images Displayed are solely for representation purposes only, All work copyright of respective owner, otherwise © 2013-2014 REVOX.
					</p>
				</div>
			</div>
			<div class="login-container bg-white">
				<div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
					<img src="{{ URL::asset('backend/img/logo.png')}}" alt="logo" data-src="{{ URL::asset('backend/img/logo.png')}}" data-src-retina="{{ URL::asset('backend/img/logo_2x.png')}}" width="78" height="22">
					<p class="p-t-35">
						Sign into your system account
					</p>
					{!! Form::open(['route' => 'Auth.authenticate','method'=>'post']) !!}
					<div class="form-group form-group-default">
						{!! Form::label('Account','Account') !!}
						<div class="controls">
							{!! Form::text('email',old('email'),['id'=>'email','class'=>'form-control','required'=>'','placeholder'=>'Your Account']) !!}
						</div>
					</div>
					<div class="form-group form-group-default">
						{!! Form::label('password','Password') !!}
						<div class="controls">
							{!! Form::password('password',['id'=>'password','class'=>'form-control','required'=>'required','placeholder'=>'Your Password']) !!}
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 no-padding">
							{!! Form::checkbox('remember',1,true,['id'=>'remember']) !!}
							{!! Form::label('','Keep Me Signed in') !!}
						</div>
						<font color="red">
						{{Session::get("message")}}
						</font>
					</div>
					{!! Form::submit('Sign in',['class'=>'btn btn-primary btn-cons m-t-10']) !!}
					{!!  $errors->first(); !!}
					{!! Form::Close() !!}
					<div class="pull-bottom sm-pull-bottom">
						<div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
							<div class="col-sm-9 no-padding m-t-10">
								<p>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@stop
