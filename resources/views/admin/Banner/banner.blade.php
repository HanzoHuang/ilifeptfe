<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<title>宜蘭縣壯圍鄉</title>

<script type="text/javascript">
	$(function(){
		var $block = $('#abgne_fade_pic'), 
			$ad = $block.find('.ad'),
			showIndex = 0,			// 預設要先顯示那一張
			fadeOutSpeed = 2000,	// 淡出的速度
			fadeInSpeed = 3000,		// 淡入的速度
			defaultZ = 10,			// 預設的 z-index
			isHover = false,
			timer, speed = 2000;	// 計時器及輪播切換的速度
		
		// 先把其它圖片的變成透明
		$ad.css({
			opacity: 0,
			zIndex: defaultZ - 1
		}).eq(showIndex).css({
			opacity: 1,
			zIndex: defaultZ
		});
		
		// 組出右下的按鈕
		var str = '';
		for(var i=0;i<$ad.length;i++){
			str += '<a href="#">' + (i + 1) + '</a>';
		}
		var $controlA = $('#abgne_fade_pic').append($('<div class="control">' + str + '</div>').css('zIndex', defaultZ + 1)).find('.control a');

		// 當按鈕被點選時
		// 若要變成滑鼠滑入來切換時, 可以把 click 換成 mouseover
		$controlA.click(function(){
			// 取得目前點擊的號碼
			showIndex = $(this).text() * 1 - 1;
			
			// 顯示相對應的區域並把其它區域變成透明
			$ad.eq(showIndex).stop().fadeTo(fadeInSpeed, 1, function(){
				if(!isHover){
					// 啟動計時器
					timer = setTimeout(autoClick, speed + fadeInSpeed);
				}
			}).css('zIndex', defaultZ).siblings('a').stop().fadeTo(fadeOutSpeed, 0).css('zIndex', defaultZ - 1);
			// 讓 a 加上 .on
			$(this).addClass('on').siblings().removeClass('on');

			return false;
		}).focus(function(){
			$(this).blur();
		}).eq(showIndex).addClass('on');

		$block.hover(function(){
			isHover = true;
			// 停止計時器
			clearTimeout(timer);
		}, function(){
			isHover = false;
			// 啟動計時器
			timer = setTimeout(autoClick, speed);
		})
		
		// 自動點擊下一個
		function autoClick(){
			if(isHover) return;
			showIndex = (showIndex + 1) % $controlA.length;
			$controlA.eq(showIndex).click();
		}
		
		// 啟動計時器
		timer = setTimeout(autoClick, speed);
	});
</script>

</head>
<body>
<div id="wrapper">
<div id="container">
      <div class="banner_bg">
        <div class="banner">
           <div id="abgne_fade_pic">
           <a href="" class="ad"><img src="images/banner.jpg" /></a>
		   <a href="" class="ad"><img src="images/banner1.jpg" /></a>
           <a href="" class="ad"><img src="images/banner1-1.jpg" /></a>
		   <a href="" class="ad"><img src="images/banner1-2.jpg" /></a>
           <a href="" class="ad"><img src="images/banner1-3.jpg" /></a>
		   <a href="" class="ad">
              <div class="news">
                 <div class="new_area"><h1><strong>公告消息</strong>-107年幸福宜蘭環保智慧王挑戰賽活動</h1><font>宜蘭縣政府訂於本(107)年9月15日(星期六)及16日(星期日)假凱旋國小視聽中心（地址：宜蘭市凱旋路130號）舉辦「107年幸福宜蘭環保智慧王挑戰賽活動」，請於107年6月1日起至「107年環境知識競賽」網站（http://www.epaee.com.tw/）踴躍報名參賽。</font></div>
                 <div class="new_area"><h1><strong>服務時間</strong>-棉花田生機園地單一徵才活動</h1><font>宜蘭縣政府訂於105年12月22日（星期四）下午1時30分至3時30分，於縣政府文康中心(宜蘭市縣政北路1號B1) 舉辦棉花田生機園地單一徵才活動，歡迎民眾踴躍參加。
詳情請洽宜蘭縣政府勞工處就業服務台！電話：03-9255130~</font></div>
                 <div class="new_area"><h1><strong>祭拜須知</strong>-107年幸福宜蘭環保智慧王挑戰賽活動</h1><font>宜蘭縣政府訂於本(107)年9月15日(星期六)及16日(星期日)假凱旋國小視聽中心（地址：宜蘭市凱旋路130號）舉辦「107年幸福宜蘭環保智慧王挑戰賽活動」，請於107年6月1日起至「107年環境知識競賽」網站（http://www.epaee.com.tw/）踴躍報名參賽。</font></div>
                 <div class="new_area"><h1><strong>法會公告</strong>-宜蘭縣政府勞工處辦理法會公告~</h1><font>宜蘭縣政府訂於106年01月12日（星期四）下午2時至4時30分，於丟丟噹森林廣場(宜蘭市宜興路一段236號)辦理聯合徵才活動，歡迎民眾踴躍參加。詳情請洽宜蘭縣政府勞工處就業服務台！電話：03-9255130
</font></div>
                 <div class="clear_line"></div>
              </div>
              <img src="images/banner2.jpg" /></a>
           <a href="" class="ad">
           <div class="news1"><div class="new_area1"><div class="event_word"><h2>精采人生 燦爛永恆</h2><h5>花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述花絮簡述</h5></div></div></div>
           <img src="images/banner3.jpg" /></a>
           </div>
        </div>
       </div>
       
     
</div>
</div>
</body>
</html>
