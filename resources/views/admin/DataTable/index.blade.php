@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!!	Html::style('css/dataTable/jquery.dataTables.min.css')  !!}
{!!	Html::script('js/dataTable/jquery.dataTables.min.js')  !!}
{!!	Html::style('backend/plugins/bootstrap-datepicker/css/datepicker3.css')  !!}
{!!	Html::script('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
{!!	Html::script('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
	<br>
@if(isset($HeadDataTableJs))
	{!!$HeadDataTableJs!!}
	<br>
@endif
@if(!isset($Add))
	@can('ACL_view',[Route::currentRouteName(),'create'])
		<a href="{!!asset($AddUrl)!!}" class="btn btn-primary btn-cons" />新增</a>
	@endcan
@endif
<br>
@if (isset($DetailName))
    <div class="alert alert-success" style="margin-top: 20px;">
        您現在所在的頁面：{{ $DetailName}}
    </div>
@endif
@if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
    </div>
@endif
{!!$DataTableJs!!}
<br>

@stop

@section('script')
<script>
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
$(document).on('click', '.ask-delete', function(e) {
	var modalElem = $('#modalSlideUp');
	var $this = $(this);
    e.preventDefault(); // does not go through with the link.
    $('#modalSlideUp').modal('show');
    $(document).on('click', '.model_delete', function(e) {
        $.ajax({
	       type: "POST",
	       url: $this.attr('href'),
	       data: {
	          _method: $this.data('method')
	       },
	       // or data: plaindata, // If 'plaindata' is an object.
	       dataType: 'json',
	       success: function(data) {
	       	  if(data==0){
	       	  	  $('#modalSlideUp_UP').modal('show');
	       	  }else{
	       	      $('#modalSlideUp_Down').modal('show');
	       	  }
	       	  $('#modalSlideUp').modal('hide');
	          console.log(data); // As moonwave99 said
	       },
	       error: function() {
	          //error condition code
	       }
    	});

    });
 });

$(document).on('click', '.model_delete_type', function(e) {
	location.reload();
});
</script>
@stop