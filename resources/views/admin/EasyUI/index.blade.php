@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!!	Html::style('Plugin/EasyUI/easyui.css')  !!}
{!! Html::style('Plugin/EasyUI/themes/icon.css')  !!}
{!!	Html::script('Plugin/EasyUI/jquery.easyui.min.js')  !!}
{!!	Html::script('js/EasyuiCRUD.js')  !!}
<?php
echo $EasyuiTtile;
//echo $EasyuiIntroduction;
echo $EasyuiTopBar;
echo $EasyuiFunctionBar;
echo $EasyuiBody;
?>
@stop

