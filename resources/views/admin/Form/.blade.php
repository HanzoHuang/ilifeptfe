@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!!	Html::script('/js/ckeditor/ckeditor.js')  !!}
{!! Html::style('backend/plugins/bootstrap-datepicker/css/datepicker3.css')  !!}
{!! Html::script('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
{!!$FormBody;!!}

@stop

@section('script')
<script type="text/javascript">

@if(isset($SelectValue))
    $("#product_class_id").val(["{!!$SelectValue!!}"]).select2();
@else
    $("#product_class_id").val([]).select2();
@endif
@if(isset($SelectValue2))
    $("#product_specification_id").val({!!$SelectValue2!!}).select2();
@else
    $("#product_specification_id").val([]).select2();
@endif
</script>
@stop
