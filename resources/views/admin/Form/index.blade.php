@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))

@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!! Html::script(asset('js/ckeditor/ckeditor.js')) !!}
{!! Html::style(asset('backend/plugins/bootstrap-datepicker/css/datepicker3.css'))  !!}
{!! Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))!!}
{!! Html::script(asset('js/bootstrap-datetimepicker.js'))!!}
<link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
{!!$FormBody;!!}

@stop
@section('script')

<script type="text/javascript">
   @if(isset($SelectValue1))
        $("#activity_id").val({!!$SelectValue1!!}).select2();
    @else
        $("#activity_id").val([]).select2();
    @endif
</script>
@stop
