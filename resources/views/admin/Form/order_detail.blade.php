@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!! Html::script(asset('js/ckeditor/ckeditor.js')) !!}
{!! Html::style(asset('backend/plugins/bootstrap-datepicker/css/datepicker3.css'))  !!}
{!! Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))!!}
{!! Html::script(asset('js/bootstrap-datetimepicker.js'))!!}
<link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
<style type="text/css">
    table,th,td{
        border:solid 1px;
        text-align: center;
    }
    td{
        width:25%;
    }
</style>
<h3>訂單資訊</h3>
訂單編號:{{$order->no}}<br>
金額:{{$order->total}}<br>
運費:{{$order->freight}}<br>
狀態:{{$order->status}}<br>
付款方式:{{$order->delivery_method}}<br>
地址:{{$order->zipcode}}{{$order->county}}{{$order->district}}{{$order->add}}<br>
訂購人:{{$order->name}}<br>
email:{{$order->email}}<br>
電話:{{$order->tel}}<br>
收貨人:{{$order->recipient_name}}<br>
收貨人email:{{$order->recipient_email}}<br>
收貨人電話:{{$order->recipient_tel}}<br>
發票類型:{{$order->invoice_method}}<br>
備註:{{$order->ext}}<br>
<h3>訂單內容</h3>
<table style="border:solid 1px;width: 600px" >
    <tr>
        <th>商品名稱</th>
        <th>數量</th>
        <th>單價</th>
        <th>小計</th>
    </tr>
    <?php $total=0; ?>
    @foreach($order_detail as $details)
    <tr>
        <td>{{$details->name}}</td>
        <td>{{$details->quantity}}</td>
        <td>{{$details->price}}</td>
        <td>{{$details->price*$details->quantity}}</td>
        <?php $total+=$details->price*$details->quantity ?>
    </tr>
    @endforeach
    <tr>
        <td>運費</td>
        <td>160</td>
        <td>總計</td>
        <td>{{$total+160}}</td>
    </tr>
</table>

@stop
@section('script')
@stop
