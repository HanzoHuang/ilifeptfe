<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	    <meta charset="utf-8" />
      <meta name="csrf-token" content="{{ csrf_token() }}" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      {!! Html::style('backend/plugins/boostrapv3/css/bootstrap.min.css')  !!}
  		{!!	Html::style('backend/plugins/pace/pace-theme-flash.css')  !!}
  		{!!	Html::style('backend/plugins/font-awesome/css/font-awesome.css')  !!}
  		{!!	Html::style('backend/plugins/jquery-scrollbar/jquery.scrollbar.css')  !!}
  		{!!	Html::style('backend/plugins/bootstrap-select2/select2.css')  !!}
      {!! Html::style('js\lightbox\css\lightbox.css')  !!}
  		{!!	Html::style('backend/css/pages-icons.css')  !!}
      {!! Html::style('backend/css/pages.css')  !!}
      {!! Html::style('css/jquery.timepicker.css')  !!}
      {!! Html::style('css/bootstrap-datetimepicker.css')  !!}

      {!! Html::script('backend/js/jquery-1.7.min.js')  !!}
      {!! Html::script('backend/plugins/boostrapv3/js/bootstrap.min.js')  !!}
  	  <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-touch-fullscreen" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="default">
	    <meta content="" name="description" />
	    <meta content="" name="author" />
    <title>@yield('title')</title>
    <style type="text/css">
      .hideAll  {
        visibility:hidden;
      }
    </style>

	</head>

	 <body class="fixed-header  pace-done sidebar-visible menu-pin hideAll" id="layout_body">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <div class="sidebar-header">
        <a href="{{asset('BackHome')}}">
              {{-- <img src="{{asset('backend/img/logo_white.png')}}" alt="logo" class="brand" data-src="backend/img/logo_white.png" data-src-retina="assets/img/logo_white_2x.png" width="78" height="22"> --}}
        </a>
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"></button>
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      @include('layouts.partials.AdminNavigation')
      <!-- END SIDEBAR MENU -->
    </nav>
    <div class="page-container">
      <!-- START HEADER -->
      <div class="header ">

        <!-- START MOBILE CONTROLS -->
         <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <span class="semi-bold">
                @if(Auth::user())
                {{Auth::user()->name}}
                您好！歡迎使用本系統
                @endif
                </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li class="bg-master-lighter">
                  <a href="{{route('BackLogin.Logout')}}" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger-plus"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
            </div>
            </div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
          </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li class="bg-master-lighter">
                  <a href="#" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">

          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
            @yield('Level')
            <!-- START ROW -->
                <div style="width:100%;height:100%;overflow:auto;">
                    @yield('content')
                </div>
            <!-- END STATR ROW -->
          </div>
        </div>
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        <li class="">
          <a href="#quickview-notes" data-toggle="tab">Logout</a>
        </li>
      </ul>
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
      <!-- Tab panes -->

    </div>
    @include('layouts.partials.DeleteModals')
    @include('layouts.partials.DeleteModalUp')
    @include('layouts.partials.DeleteModalDown')

	</body>
</html>
{!! Html::script('js/lightbox/js/lightbox.js')  !!}
{!! Html::script('backend/plugins/pace/pace.min.js')  !!}
{!! Html::script('backend/plugins/modernizr.custom.js')  !!}
{!! Html::script('backend/plugins/jquery-ui/jquery-ui.min.js')  !!}
{!! Html::script('backend/plugins/jquery-scrollbar/jquery.scrollbar.min.js')  !!}
{!! Html::script('backend/plugins/bootstrap-select2/select2.min.js')  !!}
{!! Html::script('backend/js/pages.min.js')  !!}
{!! Html::script('js/jquery.timepicker.js')!!}
<script type="text/javascript">
  $(window).load(function () {
    $("#layout_body").removeClass("hideAll");
        lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });
  });
  $('#OrientationConfig').change(function(){
      id=$('#OrientationConfig').val();
      $.ajax({
          url: '{{route('Admin.OrientationData')}}',
          data:'id='+id,
          type:"get",
          dataType:'json',
          success: function(data){
              $('[name="content1"]').val(data.content1);
              $('[name="content2"]').val(data.content2);
              $('[name="hotel_content"]').val(data.content3);
              $('[name="transportation_content"]').val(data.content4);
              $('[name="activity_content"]').val(data.content5);
          },
          beforeSend:function(){
              
          },
          complete:function(){
              
          },
          error:function(xhr, ajaxOptions, thrownError){ 
              alert('錯誤');
           }
      });
  });
  </script>
@yield('script')