<?php
$User  = Auth::user();
$Menus = Session::get('Menu','');
$bool="";
?>

<div class="sidebar-menu">
  <!-- BEGIN SIDEBAR MENU ITEMS-->
  <ul class="menu-items">
    @if($Menus!=1)
      @forelse  ($Menus as $key=>$value)
        @if($key=="帳號管理")
          <li class="m-t-30">
        @else
          <li>
        @endif
          <a href="javascript:;"> <span class="title"  title='{!!$key!!}' >{{$key}}</span> <span class="arrow"></span></a>
          <span class="icon-thumbnail"><i class="{{$value[0]->font_awesome}}"></i></span>
          @foreach  ($value as $key3 => $value3)
              @if(strpos(getCurrentControllerName(),$value[$key3]->program_name) and $bool!=1)
              <?php 
              $bool=1;
              ?>
              @endif
          @endforeach
          @if($bool==1)
            <ul class="sub-menu" style="overflow: hidden; display: block;">
            <?php $bool=0?>
          @else
            <ul class="sub-menu" style="">
          @endif
          @forelse  ($value as $key2 => $value2)
            @if($value[$key2]->acl_name=='塔位查詢')
            <li >
              <a target="_blank" href='{{asset($value[$key2]->program_name)}}' title='{!!$value[$key2]->acl_name!!}'>{{$value[$key2]->acl_name}}</a>

            <span class="icon-thumbnail">{{$key2+1}}</span>
            </li>
            @else
            <li >
              {!! html::linkRoute($value[$key2]->program_name.".index", $value[$key2]->acl_name) !!}
              <span class="icon-thumbnail">{{$key2+1}}</span>
            </li>
            @endif
          @empty
            {{請聯繫管理員}}
          @endforelse
          </ul>
        </li>
        @empty
            {{請聯繫管理員}}
        @endforelse
      }
    @endif
  </ul>
  <div class="clearfix"></div>
</div>