   <div class="modal fade slide-up disable-scroll" id="modalSlideUp_Down" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <h5> <span class="semi-bold"></span></h5>
              <p class="p-b-10">下架成功！</p>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-8">
                  <div class="p-t-20 clearfix p-l-10 p-r-10">
                    <div class="pull-left">
                      <p class="bold font-montserrat text-uppercase"></p>
                    </div>
                    <div class="pull-right">
                      <p class="bold font-montserrat text-uppercase"></p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 m-t-10 sm-m-t-10">
                  <button type="button" class="btn btn-primary btn-block m-t-5 model_delete_type">確定</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>