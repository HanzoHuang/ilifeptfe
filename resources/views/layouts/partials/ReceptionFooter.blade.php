<footer id="footer">
        <div class="container">
            <div class="row justify-content-around webmap">
                <div class="col-lg-3 col-6 mb-lg-0 mb-3">
                    <ul class="">
                        <li>
                            地址<a href="{{route('Reception.Contacts')}}#map" class="pl-2">台中市大雅區科雅路28號4樓</a>
                        </li>
                        <li>
                            電話<a href="tel:04-2568-4128" class="pl-2">04-2568-4128</a>
                        </li>
                        <li>
                            傳真<span class="pl-2">04-2568-0648</span>
                        </li>
                        <li>
                            Facebook<a href="" class="pl-2">@ilifeptfe</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-6 mb-lg-0 mb-3 text-center">
                    <p class="title">PTFE薄膜</p>
                    <ul class="">
                        <li>
                            <a href="{{route('Reception.Ptfe')}}">企業專區</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-6 text-center">
                    <p class="title">產品專區</p>
                    <ul class="">
                        <li>
                            <a href="{{route('Reception.Product',[1])}}">n-PTFE 口罩系列</a>
                        </li>
                        <li>
                            <a href="{{route('Reception.Product',[1])}}">PTFE - 防水透空氣輕薄外套</a>
                        </li>
                        <li>
                            <a href="{{route('Reception.Product',[1])}}">i-life 枕頭保潔套</a>
                        </li>
                        <li>
                            <a href="{{route('Reception.Product',[1])}}">次氯酸水</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-6 text-center">
                    <p class="title">會員專區</p>
                    <ul class="">
                        <li>
                            <a href="{{route('Reception.LoginPage')}}">會員登入</a>
                        </li>
                        <li>
                            <a href="{{route('Reception.Member')}}">訂單查詢</a>
                        </li>
                        <li>
                            <a href="{{route('Reception.QA')}}">常見問題</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row align-items-center info">
                <div class="col-12 copyright">
                    <div>&copy;<span class="strong">台灣美罩科技股份有限公司</span> 2018 All Rights Reserved.
                        <span class="block"></span> Designed by <a href="http://www.sunstar-dt.com/" class="sunstar" target="_blank">SUNSTAR</a>
                    </div>
                    <div>
                        <ul>
                            <li><a href="#">隱私權條款</a></li>
                            <li><a href="#"><服務條款</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="in-btn">
	<ul>
		<li>
			<a href="https://www.facebook.com/ilifeptfe/">
				<i class="fab fa-facebook-square"></i>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fab fa-google-plus-square"></i>
			</a>
		</li>
		<li>
			<a href="contacts.html">
				<i class="fas fa-comment"></i>
			</a>
		</li>
	</ul>
</div>
<div id="side-btn">
    <span class="js-gotop" title="回至頂端">
        <i class="fas fa-chevron-circle-up"></i>
    </span>
</div>