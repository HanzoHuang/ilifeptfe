<header id="header" role="banner">
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid">
                <a href="{{route('Home.index')}}" class="logo mr-auto mr-md-0">
                    <img src="{{asset('images/logo.png')}}" class="img-fluid">
                </a>
                <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav menus ml-auto">
                        <li class="nav-item">
                            <a href="{{route('Reception.About')}}" class="nav-link">關於我們</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('Reception.News')}}" class="nav-link">最新資訊</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('Reception.Ptfe')}}" class="nav-link">PTFE薄膜</a>
                        </li>
                        <li class="nav-item sub-menu-parent">
                            <a href="{{route('Reception.Product')}}" class="nav-link">
                                產品專區<i class="fas fa-caret-down"></i>
                            </a>
                            <ul class="sub-menu" aria-labelledby="dpwn-1">
                                
                                <li>
                                    <a href="{{route('Reception.ProductList')}}">產品列表</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('Reception.QA')}}" class="nav-link">常見問題</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('Reception.Contacts')}}" class="nav-link">聯絡我們</a>
                        </li>
                    </ul>
                </div>
                    <ul class="navbar-nav flex-row ml-auto align-items-center">
                        <li class="nav-item">
                            <a href="{{route('Reception.LoginPage')}}" class="nav-link user">
                                <i class="fas fa-user"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('Reception.Cart')}}" class="nav-link cart">
                                <i class="fas fa-shopping-cart">
                                    <span class="num">
                                        <span id="cart_count">{{$cart_count}}</span>
                                    </span>
                                </i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="change_lang" style="border: 1px solid #F48120;padding:10px;margin: 5px;border-radius:25px;width: 50px;color:#6c757d" class="nav-link">@if(Session::get('lang','zh')=='zh')繁@else en @endif</a>
                        </li>
                    </ul>
            </div>
        </nav>
    </header>