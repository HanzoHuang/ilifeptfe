<!DOCTYPE html>
<html lang="zh-TW">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>台灣美罩科技 i-life</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"><meta name="description" content="台灣美罩科技致力於生產製造全球最頂級 PTFE 薄膜與國防軍用的核生化防護服。我們希望所有消費者都能享受這樣的高品質材料， 讓頂級科技守護我們的生活。" />
    <meta name="author" content="緯智數位科技 SUNSTAR" />
    <meta name="copyright" content="台灣美罩科技股份有限公司">
    <meta property="og:title" content="台灣美罩科技 i-life" />
    <meta property="og:url" content="{{asset('Home')}}" />
    <meta property="og:site_name" content="台灣美罩科技 i-life" />
    <meta property="og:description" content="台灣美罩科技致力於生產製造全球最頂級 PTFE 薄膜與國防軍用的核生化防護服。我們希望所有消費者都能享受這樣的高品質材料， 讓頂級科技守護我們的生活。" />
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="{{asset('css/favicon.ico')}}">
    <link href="{{asset('css/googleapis.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick-theme.css')}}">
        @yield('style')
    </head>
        <!--[if lt IE 9]>
           <script src="assets/js//html5shiv.min.js"></script>
           <script src="assets/js/respond.min.js"></script>
         <![endif]-->
    <body>
        @include('layouts.partials.ReceptionTop')
        @yield('content')
        @include('layouts.partials.ReceptionFooter')
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/jquery.easing.min.js')}}"></script>
        <script src="{{asset('js/popper.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/slick.min.js')}}"></script>
        <script src="{{asset('js/jquery.ellipsis.min.js')}}"></script>
        <script src="{{asset('js/parallax.min.js')}}"></script>
        <script src="{{asset('js/main.js')}}"></script>
        @yield('script')
        <script type="text/javascript">
            $('#change_lang').click(function(){
                $.ajax({
                    url: "{{ route('Reception.ChangeLang') }}",
                    data: '',
                    dataType:"html",
                    type: "get",
                    success: function(data){
                       location.reload();
                    },
                    error: function(){
                    }
                });
            })
        </script>
    </body>
</html>
