@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/about.css')}}">
@stop
@section('content')
    <div id="main" class="content">
        <div class="about">
    <section class="parallax">
        <div class="parallax-window wow fadeIn" data-wow-duration="2s" data-parallax="scroll" data-image-src="{{asset('Upload/AboutBanner/'.$about_banner->img)}}"></div>
    </section>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="#">關於美罩</a>
                </li>
            </ol>
        </nav>
        <section>
            <h4 class="title text-center wow fadeInLeft" data-wow-duration="2s">服務項目</h4>
            <div class="row">
                @foreach($about_type1 as $key =>$value)
                <div class="col-md-4 col-6 text-center wow fadeInUp" data-wow-duration="2s">
                    <div class="services">
                        <span class="icon">
                            <i class="{{ $value->icon }}"></i>
                        </span>
                        <div class="desc">
                            <h5>{{ $value->title }}</h5>
                            {!! $value->content !!}
                        </div>
                    </div>
                </div>
                @endforeach
                
            </div>
        </section>
        <section class="process text-center">
            <h4 class="title wow fadeInLeft" data-wow-duration="2s">美罩的發展</h4>
            <!-- 1 -->
            @foreach($about_type2 as $key =>$value)
            @if($key%2==0)
            <div class="row item inverted row-eq-height wow fadeIn" data-wow-duration="2s">
                <div class="col-md-4 offset-md-1 col-sm-6 col-9 order-sm-2 order-2">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{$value->title}}</h5>
                            <p class="card-text">
                                {!! $value->content !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 col-3 order-md-2 order-sm-1 order-1 process-icon">
                    <i class="fas fa-comments"></i>
                </div>
            </div>
            @else
            <!-- 2 -->
            <div class="row item row-eq-height wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                <div class="col-md-2 offset-md-5 col-sm-6 col-3 process-icon">
                    <i class="fas fa-pencil-alt"></i>
                </div>
                <div class="col-md-4 col-sm-6 col-9">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{$value->title}}</h5>
                            <p class="card-text">
                                {!! $value->content !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </section>
    </div>
</div>
    </div>
@stop
@section('script')
@stop