@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
<div id="main">
    <div class="cart">
        <div class="container">
            <section>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                        <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">購物車</a>
                        </li>
                    </ol>
                </nav>
                <!-- 結帳步驟 -->
                <div class="checkout-nav">
                    <div class="step active">
                        <span class="badge now"><span class="text">1</span></span>
                        <span class="done name">購物車</span>
                    </div>
                    <div class="step">
                        <span class="badge"><span class="text">2</span></span>
                        <span class="name">填寫資料</span>
                    </div>
                    <div class="step">
                        <span class="badge"><span class="text">3</span></span>
                        <span class="name">完成訂單</span>
                    </div>
                </div>
                <!-- 購物車 -->
                <div class="row justify-content-center cart-header">
                    <div class="col-5">商品資訊</div>
                    <div class="col-4">
                        <div class="s-price">
                            單價
                        </div>
                        <div class="quantity">
                            數量
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="s-total">
                            小計
                        </div>
                        <div class="del">
                            刪除
                        </div>
                    </div>
                </div>
                @foreach($cart as $key =>$value)
                <div class="row justify-content-center cart-row" id="product{{$key}}">
                    <div class="col-md-5 col-12 cart-item">
                            <div class="pro-img" style="background-image: url({{asset('Upload/Product/'.$value['img'])}});">
                            </div>
                            <div class="description">
                                <div>{{$value['name']}}</div>
                                {{-- <span class="help-text">商品系列名稱 / 優惠活動名稱 / 折扣額度顯示</span> --}}
                            </div>
                    </div>
                    <div class="col-md-4 col-6 cart-quantity">
                        <div class="s-price">
                            <span data-title="單價" class="hint price">{{$value['price']}}</span>
                        </div>
                        <div class="quantity">
                            <div class="input-group">
                                <span class="input-group-btn input-group-prepend" disabled="disabled" data-type="minus">
                                    <button type="button" class="btn btn-outline increase"  pod-id="{{$key}}">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </span>
                                <input type="text" name="cart-quantity" id="quantity{{$key}}" class="form-control text-center" value="{{$value['quantity']}}" min="1" max="10" />
                                <span class="input-group-btn input-group-append" data-type="plus">
                                    <button type="button" class="btn btn-outline reduce"  pod-id="{{$key}}">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 cart-action">
                        <div class="s-total">
                            <span data-title="小計" class="hint price" id="price{{$key}}">{{$value['price']*$value['quantity']}}</span>
                        </div>
                        <div class="del" pod-id="{{$key}}">
                            <span>
                                <i class="fas fa-trash"></i>
                            </span>
                        </div>
                    </div>
                </div>
                @endforeach
                <div id="total">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>小計：</td>
                                <td class="price" >
                                    <span id="total_span">{{$total}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>運費：</td>
                                <td class="price">
                                    <span>160</span>
                                </td>
                            </tr>
                            <tr class="total">
                                <td>合計：</td>
                                <td class="price">
                                    <span id="total_price">{{$total+160}}</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-11 col-12">
                        <div class="act-btn">
                            <div class="left">
                                <a href="{{route('Reception.ProductList')}}" class="btn btn-none">
                                    繼續購物
                                </a>
                            </div>
                            <div class="right">
                                <a href="{{route('Reception.Checkout')}}" class="btn btn-primary">
                                    前往結帳
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
    //增加商品數量
    $('.increase').click(function(){
        pod_id=$(this).attr('pod-id');
        qty=parseInt($('#quantity'+pod_id).val());
        $('#quantity'+pod_id).val(qty+1);
        EditQuantity(pod_id,1);
    });
    //減少商品數量
    $('.reduce').click(function(){
        pod_id=$(this).attr('pod-id');
        qty=parseInt($('#quantity'+pod_id).val());
        //減少為0等於刪除
        if(qty-1<1){
            del(pod_id);
            return '';
        }
        //type 1增加 2減少
        EditQuantity(pod_id,2);
        $('#quantity'+pod_id).val(qty-1);

    });
    function EditQuantity(pod_id,type) {
        $.ajax({
            url: '{{route('Reception.EditQuantity')}}',
            data:{product_id:pod_id,type:type},
            type:"get",
            dataType:'json',
            success: function(data){
                $('#price'+pod_id).html(data['this_price']);
                $('#total_span').html(data['total']);
                $('#total_price').html(data['total']+160);
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('錯誤');
             }
        });
    }
    $('.del').click(function(){
        pod_id=$(this).attr('pod-id');
        del(pod_id);
    });
    function del(pod_id) {
        if(confirm('確定要刪除商品?')){
                $.ajax({
                    url: '{{route('Reception.DelCart')}}',
                    data:{product_id:pod_id},
                    type:"get",
                    dataType:'json',
                    success: function(data){
                        $('#product'+pod_id).remove();
                        $('#total_span').html(data['total']);
                        $('#total_price').html(data['total']+160);
                    },
                    beforeSend:function(){
                        
                    },
                    complete:function(){
                        
                    },
                    error:function(xhr, ajaxOptions, thrownError){ 
                        alert('錯誤');
                     }
                });
            }
    }
</script>
@stop