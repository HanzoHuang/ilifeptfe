@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
<div id="main">
    <div class="cart checkout">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                    <li class="breadcrumb-item"><a href="cart.html">購物車</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">填寫資料</a>
                    </li>
                </ol>
            </nav>
            <!-- 結帳步驟 -->
            <div class="checkout-nav">
                <div class="step active">
                    <span class="badge now">
                        <span class="text">1</span>
                    </span>
                    <span class="done name">購物車</span>
                </div>
                <div class="step active">
                    <span class="badge now">
                        <span class="text">2</span>
                    </span>
                    <span class="done name">填寫資料</span>
                </div>
                <div class="step">
                    <span class="badge">
                        <span class="text">3</span>
                    </span>
                    <span class="name">完成訂單</span>
                </div>
            </div>
            <form method="post" action="{{route('Reception.Done')}}" >
            {{ csrf_field() }}
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-10 col-12">
                    <div class="accordion mb-4" id="cart-check">
                        <div class="card">
                            <div class="card-header" id="ckh">
                                <h6 class="mb-0" data-toggle="collapse" data-target="#ckc" aria-expanded="true" aria-controls="ckc">
                                     共<span class="pieces">{{$cart_count}}</span>商品，總計<span class="price strong">{{$total}}</span>元<span class="icon"><i class="fas fa-angle-down"></i></span>
                                </h6>
                            </div>
                            <div id="ckc" class="collapse" aria-labelledby="ckh" data-parent="#qa">
                                <div class="card-body">
                                    @foreach($cart as $key =>$value)
                                    <div class="row justify-content-center cart-row">
                                        <div class="col-md-5 col-12 cart-item">
                                            <div class="pro-img" style="background-image: url({{asset('Upload/Product/'.$value['img'])}});">
                                            </div>
                                            <div class="description">
                                                <div>{{$value['name']}}</div>
                                                <span class="help-text">尺寸:{{$value['size']}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-6 cart-quantity">
                                            <div class="s-price">
                                                <span class="price">{{$value['price']}}</span>
                                            </div>
                                            <div class="quantity">
                                                <span class="amount">{{$value['quantity']}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 cart-action">
                                            <div class="s-total">
                                                <span class="price">{{$value['price']*$value['quantity']}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div id="total">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>小計：</td>
                                                    <td class="price">
                                                        <span>{{$total}}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>運費：</td>
                                                    <td class="price">
                                                        <span>160</span>
                                                    </td>
                                                </tr>
                                                <tr class="total">
                                                    <td>合計：</td>
                                                    <td class="price">
                                                        <span>{{$total+160}}</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="card mb-4">
                        <div class="card-header">
                            付款與運送地址
                        </div>
                        <div class="card-body">
                            <div name="delivery-form-1" class="row" method="post" action="{{route('Reception.Done')}}" >
                                <div class="col-sm-6 col-12 b-r">
                                    <div class="form-group">
                                        <label for="order-delivery-method">付款方式</label>
                                        <select name="delivery_method" id="order-delivery-method" class="custom-select cus-form">
                                            <option selected disabled>請選擇付款方式</option>
                                            <option value="1">貨到付款</option>
                                            <option value="2">線上刷卡</option>
                                            <option value="3">銀行匯款</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                        <label for="twzipcode">收件地區</label>
                                        <div id="twzipcode" class="form-group">
                                            <div class="tw-city">
                                                <div class="" name="county" data-role="county" data-name="county" data-style="county-style"></div>
                                            </div>
                                            <div class="tw-city">
                                                <div class="" name="district" data-role="district" data-name="district" data-style="district-style"></div>
                                                <div data-role="zipcode" data-name="zipcode" data-style="zipcode-style" style="display: none;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="add" class="control-label">詳細地址
                                        </label>
                                        <input id="add" type="text" class="cus-form" name="add" value="" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header">
                            填寫收件資料
                        </div>
                        <div class="card-body">
                            <div name="delivery-form-2" class="row">
                                <div class="col-sm-6 col-12 b-r order-1">
                                    <div class="form-group">
                                        <label for="order-name" class="control-label">訂購人姓名
                                        </label>
                                        <input id="order-name" name="name" type="text" class="cus-form" name="order-name" value="" required/>
                                    </div>
                                    <div class="form-group">
                                        <label for="order-mail" class="control-label">訂購人信箱
                                        </label>
                                        <input id="order-mail" name="email" type="email" class="cus-form" name="order-mail" value="" required/>
                                    </div>
                                    <div class="form-group">
                                        <label for="order-tel" class="control-label">訂購人電話</label>
                                        <input id="order-tel" type="tel" name="tel"  class="cus-form" name="order-tel" value="" required/>
                                    </div>
                                </div>
                                {{-- <div class="col-12 order-2 order-sm-first">
                                    <div class="form-group form-check help-text">
                                        <input type="checkbox" class="form-check-input test" id="delivery-data" name="delivery-data">
                                        <label class="form-check-label" for="delivery-data">
                                            <span class="checkbox">收件人資料與訂購人資料相同</span>
                                        </label>
                                    </div>
                                </div> --}}
                                <div class="col-sm-6 col-12 order-3">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">收件人姓名
                                        </label>
                                        <input id="recipient-name" name="recipient_name" type="text" class="cus-form" name="recipient-name" value="" required/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-mail" class="control-label">訂購人信箱
                                        </label>
                                        <input id="recipient-mail"  name="recipient_email" type="email" class="cus-form" name="recipient-mail" value="" required/>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-tel" class="control-label">收件人電話</label>
                                        <input id="recipient-tel" name="recipient_tel" type="tel" class="cus-form" name="recipient-tel" value="" required/>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-12 order-last">
                                    <div class="form-group invoice">
                                        <label for="invoice-method">發票類型</label>
                                        <select name="invoice_method" id="invoice-method" class="custom-select cus-form">
                                            <option value="1">二聯式發票</option>
                                            <option value="2">三聯式發票</option>
                                        </select>
                                        <div class="inv- inv-2">
                                            <input type="text" name="company" placeholder="公司抬頭" class="cus-form">
                                            <input type="text" placeholder="統一編號" name="uniform_numbers" class="cus-form">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            備註
                        </div>
                        <div class="card-body">
                            <div name="delivery-form-3" class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="cus-form" id="msg" name="ext" placeholder="訂單備註"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="total">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>小計：</td>
                                    <td class="price">
                                        <span>{{$total}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>運費：</td>
                                    <td class="price">
                                        <span>160</span>
                                    </td>
                                </tr>
                                <tr class="total">
                                    <td>合計：</td>
                                    <td class="price">
                                        <span>{{$total+160}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-10 col-md-11 col-12">
                    <div class="act-btn">
                        <div class="left">
                            <a href="{{route('Reception.Cart')}}" class="btn btn-none" >
                                返回購物車
                            </a>
                        </div>
                        <div class="right">
                            <input type="submit" class="btn btn-primary" value="前往結帳">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        </div>
    </section>
</div>
    </div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#twzipcode').twzipcode({
            'detect': true
        });
        $('#invoice-method').change(function() {
            $('.inv-').hide();
            $('.inv-' + $(this).val()).show("slow");
        }).change();
    });
</script>
@stop