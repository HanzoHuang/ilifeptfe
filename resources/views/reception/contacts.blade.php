@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/about.css')}}">
@stop
@section('content')
    <div id="main" class="content">
        <div class="contacts">
    <div class="banner wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('images/main.jpg')}});" title="">
    </div>
    <div class="container">
        <section>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">聯絡我們</a>
                    </li>
                </ol>
            </nav>
            <div class="row justify-content-around">
                <div class="col-lg-5 col-12 mb-md-0 mb-3 info wow fadeInLeft" data-wow-duration="2s">
                    <ul class="">
                        <li>
                            <span>地址</span>
                            <p>{{$contact_config->address}}</p>
                        </li>
                        <li>
                            <span>電話</span>
                            <p><a href="tel:{{$contact_config->tel}}">{{$contact_config->tel}}</a></p>
                        </li>
                        <li>
                            <span>傳真</span>
                            <p>{{$contact_config->fax}}</p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-12 wow fadeIn" data-wow-duration="2s">
                    <form id="contacts">
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-12 col-form-label">姓名</label>
                            <div class="col-md-9 col-12">
                                <input type="text" class="cus-form" id="name" placeholder="*必填" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-md-3 col-12 col-form-label spec">電話</label>
                            <div class="col-md-9 col-12">
                                <input type="tel" class="cus-form" id="tel" placeholder="*必填" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-12 col-form-label">電子郵件</label>
                            <div class="col-md-9 col-12">
                                <input type="email" class="cus-form" id="email" placeholder="*必填" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="msg" class="col-md-3 col-12 col-form-label">洽詢內容</label>
                            <div class="col-md-9 col-12">
                                <textarea class="cus-form" id="msg" placeholder="備註"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-12 col-form-label">驗證碼</label>
                            <div class="col-md-4 col-5">
                                <input type="text" class="cus-form" id="name" placeholder="*必填" required="">
                            </div>
                            <div class="col-md-5 col-7 pl-2">
                                <span style="display: inline-block; width: 150px; height: 40px; background: #eee;"></span>
                                <span class="verify">更換驗證碼</span>
                            </div>
                        </div>
                        <div class="text-right">
                            <input type="submit" class="btn btn-primary" name="send" value="確認送出">
                        </div>
                    </form>
                </div>
                <div class="col-12 wow fadeIn mt-4" data-wow-duration="2s">
                    <div id="map"></div>
                </div>
            </div>
        </section>
    </div>
</div>
    </div>
@stop
@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8W3IS9FqG8DvDR6ZsZ2xFGqDk5yfwR-c"></script>
    <script src="{{asset('js/google_map.js')}}"></script>
@stop