@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
    <div id="main">
        <div class="cart done">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item"><a href="">購物車</a></li>
                    <li class="breadcrumb-item"><a href="">填寫資料</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">完成訂單</a>
                    </li>
                </ol>
            </nav>
            <!-- 結帳步驟 -->
            <div class="checkout-nav">
                <div class="step active">
                    <span class="badge now">
                        <span class="text">1</span>
                    </span>
                    <span class="done name">購物車</span>
                </div>
                <div class="step active">
                    <span class="badge now">
                        <span class="text">2</span>
                    </span>
                    <span class="done name">填寫資料</span>
                </div>
                <div class="step active">
                    <span class="badge now">
                        <span class="text">3</span>
                    </span>
                    <span class="name">完成訂單</span>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-10 col-12">
                    <h5>訂購完成！</h5>
                    <h6>訂單編號：<span>{{$order['no']}}</span></h6>
                    @foreach($cart as $key =>$value)
                    <div class="row justify-content-center cart-row">
                        <div class="col-md-5 col-12 cart-item">
                            <div class="pro-img" style="background-image: url({{asset('Upload/Product/'.$value['img'])}});">
                            </div>
                            <div class="description">
                                <div>{{$value['name']}}</div>
                                <span class="help-text">{{$value['size']}}</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 cart-quantity">
                            <div class="s-price">
                                <span class="price">${{$value['price']}}</span>
                            </div>
                            <div class="quantity">
                                <span class="amount">{{$value['quantity']}}</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-6 cart-action">
                            <div class="s-total">
                                <span class="price">{{$value['price']*$value['quantity']}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div id="total">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>小計：</td>
                                    <td class="price">
                                        <span>{{$order['total']}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>運費：</td>
                                    <td class="price">
                                        <span>{{$order['freight']}}</span>
                                    </td>
                                </tr>
                                <tr class="total">
                                    <td>合計：</td>
                                    <td class="price">
                                        <span>{{$order['total']+$order['freight']}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                        <div class="card mt-4 mb-4">
                            <div class="card-header">
                                訂單資訊
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><span>付款方式</span>
                                    @if($order['delivery_method']==1)
                                        <p>貨到付款</p>
                                    @elseif($order['delivery_method']==2)
                                        <p>線上刷卡</p>
                                    @elseif($order['delivery_method']==3)
                                        <p>銀行匯款</p>
                                    @endif
                                </li>
                                <li class="list-group-item"><span>收件地址</span>
                                    <p>{{$order['add']}}</p>
                                </li>
                                <li class="list-group-item"><span>訂購人姓名</span>
                                    <p>{{$order['name']}}</p>
                                </li>
                                <li class="list-group-item"><span>訂購人信箱</span>
                                    <p>{{$order['email']}}</p>
                                </li>
                                <li class="list-group-item"><span>訂購人電話</span>
                                    <p>{{$order['tel']}}</p>
                                </li>
                                <li class="list-group-item"><span>收件人名稱</span>
                                    <p>{{$order['recipient_name']}}</p>
                                </li>
                                <li class="list-group-item"><span>收件人信箱</span>
                                    <p>{{$order['recipient_email']}}</p>
                                </li>
                                <li class="list-group-item"><span>收件人電話</span>
                                    <p>{{$order['recipient_tel']}}</p>
                                </li>
                                <li class="list-group-item"><span>發票類型</span>
                                    @if($order['invoice_method']==1)
                                    <p>二聯式發票</p>
                                    @else
                                    <p>三聯式發票</p>
                                    @endif
                                </li>
                                <li class="list-group-item"><span>備註</span>
                                    <p>{{$order['ext']}}</p>
                                </li>
                            </ul>
                        </div>
                </div>
            </div>
            <div class="text-center">
                <a href="Home" class="btn btn-outline">回首頁</a>
            </div>
        </div>
    </section>
</div>
    </div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
$(document).ready(function() {
    $('#twzipcode').twzipcode({
        'detect': true
    });
    $('#invoice-method').change(function() {
        $('.inv-').hide();
        $('.inv-' + $(this).val()).show("slow");
    }).change();
});
</script>
@stop