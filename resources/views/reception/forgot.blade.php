@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
<div class="modal fade" id="forgot" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">忘記密碼</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>請輸入您註冊帳號時填寫的信箱，我們將發送新密碼到您的信箱！</p>
                <form action="#">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                <input type="submit" id="submit" class="btn btn-secondary" data-dismiss="modal" value="確認送出">
            </div>
        </div>
    </div>
</div>
    <div id="main">
        <div class="member">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                    <li class="breadcrumb-item"><a href="member.html">會員專區</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">忘記密碼</a>
                    </li>
                </ol>
            </nav>
            <div class="logo">
                <img src="images/logo.png" class="img-fluid">
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6 col-12 border wow fadeIn" data-wow-duration="2s">
                    <form action="#" id="login" class="pb-4 mb-md-0 mb-5">
                        <h4 class="title text-center mb-5 mt-4">忘記密碼</h4>
                        <div class="form-group row">
                            <label for="account" class="col-lg-3 col-md-4 col-3 col-form-label">Email</label>
                            <div class="col-lg-9 col-md-8 col-9">
                                <input type="email" class="cus-form" id="account" name="account" placeholder="請輸入您的Email" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tel" class="col-lg-3 col-md-4 col-3 col-form-label">電話</label>
                            <div class="col-lg-9 col-md-8 col-9">
                                <input type="tel" class="cus-form" id="tel" name="tel" placeholder="請輸入您的電話" required />
                            </div>
                        </div>
                        <div class="form-group row justify-content-center mt-5">
                            <div class="col-6">
                                <input type="submit" value="查詢密碼" class="btn btn-primary w-100" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
    </div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#twzipcode').twzipcode({
            'detect': true
        });
        $('#invoice-method').change(function() {
            $('.inv-').hide();
            $('.inv-' + $(this).val()).show("slow");
        }).change();
    });
</script>
@stop