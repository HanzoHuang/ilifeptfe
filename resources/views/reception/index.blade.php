@extends('layouts.reception.reception')
@section('style')
    <link rel="stylesheet" href="{{asset('css/index.css')}}">
@stop
@section('content')
{{-- {{ trans('messages.about') }} --}}
    <div id="main">
        <div class="index">
            <div class="banner wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/IndexConfig/'.$index_config->banner_img)}});" title=""></div>
            <div class="container">
                <section>
                    <div class="row about">
                        <div class="col-md-5 col-12 intro order-md-2 wow fadeIn" data-wow-duration="2s">
                            <div class="text">
                                {!! $index_config->about_content !!}
                            </div>
                        </div>
                        <div class="col-md-7 col-12 img order-md-1 wow fadeIn" data-wow-duration="2s">
                            <img src="{{asset('Upload/IndexConfig/'.$index_config->about_img)}}" class="img-fluid" alt="">
                        </div>
                    </div>
                </section>
                <section>
                    <h4 class="text-center">{{trans('messages.1')}}</h4>
                    <div class="row news">
                        <div class="col-md-6 col-12 wow fadeIn" data-wow-duration="2s">
                            @if(isset($news[0]))
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
            	                   <div class="img" style="background-image: url({{asset('Upload/News/'.$news[0]->img)}});" title="">
            	                   </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[$news[0]->id])}}">
                                        <span class="date">{{$news[0]->date}}</span>
                                        <span class="news-title">{{ $news[0]->title }}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @if(isset($news[1]))
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                   <div class="img" style="background-image: url({{asset('Upload/News/'.$news[1]->img)}});" title="">
                                   </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[$news[1]->id])}}">
                                        <span class="date">{{$news[1]->date}}</span>
                                        <span class="news-title">{{ $news[1]->title }}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6 col-12 wow fadeIn" data-wow-duration="2s">
                            @if(isset($news[2]))
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                   <div class="img" style="background-image: url({{asset('Upload/News/'.$news[2]->img)}});" title="">
                                   </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[$news[2]->id])}}">
                                        <span class="date">{{$news[2]->date}}</span>
                                        <span class="news-title">{{ $news[2]->title }}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @if(isset($news[3]))
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                   <div class="img" style="background-image: url({{asset('Upload/News/'.$news[3]->img)}});" title="">
                                   </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[$news[3]->id])}}">
                                        <span class="date">{{$news[3]->date}}</span>
                                        <span class="news-title">{{ $news[3]->title }}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
            <section class="pb-0">
                <div class="container-fluid pl-0 pr-0">
                    <div class="row no-gutters row-eq-height products wow fadeIn" data-wow-duration="2s">
                        <div class="col-md-6 col-12 cover" style="background-image: url({{asset('Upload/IndexConfig/'.$index_config->introduction_img1)}});" title="">
                        </div>
                        <div class="col-md-6 col-12 overlay">
                            <div class="text">
                               {!! $index_config->introduction_content1 !!}
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters row-eq-height products wow fadeIn" data-wow-duration="2s">
                        <div class="col-md-6 col-12 overlay">
                            <div class="text">
                               {!! $index_config->introduction_content2 !!}
                            </div>
                        </div>
                        <div class="col-md-6 col-12 cover" style="background-image: url({{asset('Upload/IndexConfig/'.$index_config->introduction_img2)}});" title="">
                        </div>
                    </div>
                   
                </div>
            </section>
        </div>
    </div>
@stop
@section('script')

@stop