@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
    <div class="modal fade" id="forgot" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">忘記密碼</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>請輸入您註冊帳號時填寫的信箱，我們將發送新密碼到您的信箱！</p>
                <form action="#">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                <input type="submit" id="submit" class="btn btn-secondary" data-dismiss="modal" value="確認送出">
            </div>
        </div>
    </div>
</div>
   
</nav>
    </header>
    <div id="main">
        <div class="member">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">登入會員</a>
                    </li>
                </ol>
            </nav>
            <ul class="nav nav-tabs" id="login-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="login-1-tab" data-toggle="tab" href="#login-1" role="tab" aria-controls="login-1" aria-selected="true">登入會員</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="login-2-tab" data-toggle="tab" href="#login-2" role="tab" aria-controls="login-2" aria-selected="false">訂單查詢</a>
                </li>
            </ul>
            <div class="tab-content wow fadeIn" data-wow-duration="2s" id="login-tab-content">
                <div class="tab-pane fade show active" id="login-1" role="tabpanel" aria-labelledby="login-1-tab">
                    <div class="logo">
                        <img src="images/logo.png" class="img-fluid">
                    </div>
                    <div class="row justify-content-center justify-content-md-around">
                        <div class="col-lg-5 col-md-6 col-12 border wow fadeIn" data-wow-duration="2s">
                            <form action="{{route('Reception.RLogin')}}" method="post" id="login" class="pb-4 mb-md-0 mb-5">
                                {{ csrf_field() }}
                                <h4 class="title text-center mb-5 mt-4">登入會員</h4>
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <label for="email" class="col-lg-3 col-md-4 col-3 col-form-label">帳號</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="text" class="cus-form" id="account" name="account" placeholder="" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pwd" class="col-lg-3 col-md-4 col-3 col-form-label">密碼</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="password" class="cus-form" id="password" name="password" placeholder="" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <p class="ps">
                                            <a href="{{route('Reception.Forgot')}}" class="">忘記密碼</a>
                                            <span class="social"><a href="#social-login" class="">社群帳號登入</a></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center mt-5">
                                    <div class="col-6 text-center">
                                        <input type="submit" value="登入" class="btn btn-primary" onclick="javascript:location.href='{{route('Reception.Member')}}'" />
                                    </div>
                                    <div class="col-6 text-center">
                                        <input type="button" value="註冊" class="btn btn-secondary" onclick="javascript:location.href='{{route('Reception.Registar')}}'" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-5 col-md-6 col-12 border wow fadeInLeft" data-wow-duration="2s">
                            <div id="social-login" class="pb-4">
                                <h4 class="title text-center mb-5 mt-4">快速登入<span class="">使用社群帳號登入</span></h4>
                                <div class="login-btn">
                                    <a href="{{route('Auth.oauthLogin',['facebook'])}}" class="btn btn-1">Facebook</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="login-2" role="tabpanel" aria-labelledby="login-2-tab">
                    <div class="logo">
                        <img src="images/logo.png" class="img-fluid">
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-6 col-12 border wow fadeIn" data-wow-duration="2s">
                            <form action="#" id="login" class="pb-4 mb-md-0 mb-5">
                                <h4 class="title text-center mb-5 mt-4">訂單查詢</h4>
                                <div class="form-group row">
                                    <label for="order-num" class="col-lg-3 col-md-4 col-3 col-form-label">訂單編號</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="text" class="cus-form" id="order-num" name="order-num" placeholder="例：#1201212012" required />
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center mt-5">
                                    <div class="col-6">
                                        <input type="submit" value="查詢" class="btn btn-primary w-100" onclick="javascript:location.href='{{route('Reception.OrderDetail',[1])}}'" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    </div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#twzipcode').twzipcode({
            'detect': true
        });
        $('#invoice-method').change(function() {
            $('.inv-').hide();
            $('.inv-' + $(this).val()).show("slow");
        }).change();
    });
</script>
@stop