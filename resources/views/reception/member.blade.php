@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
    <div class="modal fade" id="forgot" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">忘記密碼</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>請輸入您註冊帳號時填寫的信箱，我們將發送新密碼到您的信箱！</p>
                <form action="#">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                <input type="submit" id="submit" class="btn btn-secondary" data-dismiss="modal" value="確認送出">
            </div>
        </div>
    </div>
</div>
    <div id="main">
        <div class="member">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">會員專區</a>
                    </li>
                </ol>
            </nav>
            <ul class="nav nav-tabs" id="mem-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="mem-1-tab" data-toggle="tab" href="#mem-1" role="tab" aria-controls="mem-1" aria-selected="true">交易紀錄</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="mem-2-tab" data-toggle="tab" href="#mem-2" role="tab" aria-controls="mem-2" aria-selected="false">會員資料</a>
                </li>
            </ul>
            <div class="tab-content wow fadeIn" data-wow-duration="2s" id="mem-tab-content">
                <div class="tab-pane fade show active" id="mem-1" role="tabpanel" aria-labelledby="mem-1-tab">
                    <div class="row justify-content-center mem-item">
                        <div class="col-12 wow fadeIn" data-wow-duration="2s">
                            <table class="table table-bordered table-hover rwd-table" id="order">
    <tbody>
        <tr class="tr-only-hide">
            <th width="8%">順序</th>
            <th width="20%">訂單編號</th>
            <th width="15%">購買人</th>
            <th width="30%">地址</th>
            <th width="15%">金額</th>
            <th width="13%">功能</th>
        </tr>
        @foreach($order as $key =>$value)
        <tr>
            <td data-th="順序">{{$key+1}}</td>
            <td data-th="訂單編號">{{$value->no}}</td>
            <td data-th="購買人">{{$value->name}}</td>
            <td data-th="地址">{{$value->add}}</td>
            <td data-th="金額">${{$value->total+$value->freight}}</td>
            <td data-th="管理"><a href="{{route('Reception.OrderDetail',[$value->id])}}">查看</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="mem-2" role="tabpanel" aria-labelledby="mem-2-tab">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-6 col-12 border wow fadeIn" data-wow-duration="2s">
                            <form action="{{route('Reception.MemberDataUpdate')}}" method="post" id="login" class="pb-4 mb-md-0 mb-5">
                                {{ csrf_field() }}
                                <h4 class="title text-center mb-5 mt-4">更改會員資料</h4>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <label for="account" class="col-lg-3 col-md-4 col-3 col-form-label">帳號</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="text" class="form-control-plaintext" id="account" name="account" placeholder="" readonly="" value="{{Session::get('member')->account}}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="o-pwd" class="col-lg-3 col-md-4 col-3 col-form-label">現用密碼</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="password" class="cus-form" id="password" name="old_password" placeholder="請輸入現用密碼"  />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pwd" class="col-lg-3 col-md-4 col-3 col-form-label">新密碼</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="password" class="cus-form" id="password" name="password" placeholder="新密碼" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ck-pwd" class="col-lg-3 col-md-4 col-3 col-form-label">確認新密碼</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="password" class="cus-form" id="ck_password" name="ck_password" placeholder="再次輸入新密碼"  />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-lg-3 col-md-4 col-3 col-form-label">姓名</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="text" class="cus-form" id="name" name="name" placeholder="" required value="{{Session::get('member')->name}}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-lg-3 col-md-4 col-3 col-form-label">Email</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="email" class="cus-form" id="email" name="email" placeholder="" required value="{{Session::get('member')->email}}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tel" class="col-lg-3 col-md-4 col-3 col-form-label">電話</label>
                                    <div class="col-lg-9 col-md-8 col-9">
                                        <input type="tel" class="cus-form" id="tel" name="tel" placeholder="" required value="{{Session::get('member')->tel}}" />
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center mt-5">
                                    <div class="col-6">
                                        <input type="submit" value="確認更改" class="btn btn-primary w-100" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    </div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
$(document).ready(function() {
    $('#twzipcode').twzipcode({
        'detect': true
    });
    $('#invoice-method').change(function() {
        $('.inv-').hide();
        $('.inv-' + $(this).val()).show("slow");
    }).change();
});
</script>
@stop