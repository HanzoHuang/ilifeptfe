@extends('layouts.reception.reception')
@section('style')
    <link rel="stylesheet" href="{{asset('css/about.css')}}">
@stop
@section('content')
    <div id="main" class="content">
        <div class="news">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">最新資訊</a>
                    </li>
                </ol>
            </nav>
            <ul class="nav nav-tabs" id="news-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link @if(Input::get('type',1)==1) active show @endif" id="news-1-tab" data-toggle="tab" href="#news-1" role="tab" aria-controls="news-1" aria-selected="true">最新資訊</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(Input::get('type',1)==2) active show @endif"" id="news-2-tab" data-toggle="tab" href="#news-2" role="tab" aria-controls="news-2" aria-selected="false">展場資訊</a>
                </li>
            </ul>
            <div class="tab-content wow fadeIn" data-wow-duration="2s" id="news-tab-content">
                <div class="tab-pane fade @if(Input::get('type',1)==1) active show @endif" id="news-1" role="tabpanel" aria-labelledby="news-1-tab">
                    <div class="row justify-content-center news-item">
                        <div class="col-md-6 col-12 wow fadeInUp" data-wow-duration="2s">
                            @foreach($news_type1 as $key =>$value)
                            @if($key%2==0)
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                	<div class="img" style="background-image: url({{asset('Upload/News/'.$value->img)}});" title="">
                                	</div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[$value->id])}}">
                                        <span class="date">{{$value->date}}</span>
                                        <span class="news-title">{{$value->title}}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="col-md-6 col-12 wow fadeInUp" data-wow-duration="2s">
                            @foreach($news_type1 as $key =>$value)
                            @if($key%2==1)
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                    <div class="img" style="background-image: url({{asset('Upload/News/'.$value->img)}});" title="">
                                    </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[$value->id])}}">
                                        <span class="date">{{$value->date}}</span>
                                        <span class="news-title">{{$value->title}}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                            {!! $news_type1->appends(['type'=>1])->render() !!}
                    </div>
                </div>
                <div class="tab-pane fade @if(Input::get('type',1)==2) active show @endif" id="news-2" role="tabpanel" aria-labelledby="news-2-tab">
                    <div class="row justify-content-center news-item">
                        <div class="col-md-6 col-12 wow fadeInUp" data-wow-duration="2s">
                            @foreach($news_type2 as $key =>$value)
                            @if($key%2==0)
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                    <div class="img" style="background-image: url({{asset('Upload/News/'.$value->img)}});" title="">
                                    </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[1])}}">
                                        <span class="date">{{$value->date}}</span>
                                        <span class="news-title">{{$value->title}}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="col-md-6 col-12 wow fadeInUp" data-wow-duration="2s">
                            @foreach($news_type2 as $key =>$value)
                            @if($key%2==1)
                            <div class="row no-gutters item align-items-center">
                                <div class="col-3 cover">
                                    <div class="img" style="background-image: url({{asset('Upload/News/'.$value->img)}});" title="">
                                    </div>
                                </div>
                                <div class="col-9 title">
                                    <a href="{{route('Reception.NewsDetail',[1])}}">
                                        <span class="date">{{$value->date}}</span>
                                        <span class="news-title">{{$value->title}}</span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                            {!! $news_type2->appends(['type'=>2])->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    </div>
@stop
@section('script')   
    <script type="text/javascript">
        $(document).ready(function() {
            $('.parallax-window').parallax({imageSrc: '{{asset('images/about/IMG_0652.jpg')}}'});
            $('.news-title').ellipsis({
                row: 2
            });
        });
    </script>
@stop
