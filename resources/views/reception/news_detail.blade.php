@extends('layouts.reception.reception')
@section('style')
    <link rel="stylesheet" href="{{asset('css/about.css')}}">
@stop
@section('content')
<div id="main" class="content">
    <div class="news">
        <section>
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                        <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                        <li class="breadcrumb-item"><a href="{{route('Reception.News')}}">最新資訊</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">資訊內文</a>
                        </li>
                    </ol>
                </nav>
                <div class="row justify-content-center">
                    <div class="col-md-10 col-11 wow fadeInUp" data-wow-duration="2s">
                        <article class="news-de">
                            <h5 class="title">{{$news->title}}</h5>
                            <p class="date">{{$news->date}}</p>
                            <div class="img" style="background-image: url({{asset('Upload/News/'.$news->img)}});">
                            </div>
                            <div class="text">
                                {!! $news->content !!}
                            </div>
                        </article>
                        <div class="text-center">
                            <a href="{{route('Reception.News')}}" class="btn btn-outline">返回最新資訊</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop
@section('script')   
@stop
