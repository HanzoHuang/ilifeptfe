@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/member.css')}}">
@stop
@section('content')
    <div id="main">
        <div class="cart done">
    <section>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item"><a href="{{route('Reception.Member')}}">會員專區</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">查看訂單</a>
                    </li>
                </ol>
            </nav>
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-10 col-12">
                    <div class="situation">
                        <h5>訂單狀態：
                            <span>
                                @if($order->status==100)
                                尚未付款
                                @elseif($order->status==200)
                                已付款
                                @elseif($order->status==300)
                                已出貨
                                @endif
                            </span>
                        </h5>
                        <h6>訂單內容</h6>
                    </div>
                    @foreach($order_detail as $details)
                    <div class="row justify-content-center cart-row">
                        <div class="col-md-5 col-12 cart-item">
                            <div class="pro-img" style="background-image: url(images/about/IMG_0652.jpg);">
                            </div>
                            <div class="description">
                                <div>{{$details->name}}</div>
                                <span class="help-text">尺寸:{{$details->size}}</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 cart-quantity">
                            <div class="s-price">
                                <span class="price">{{$details->price}}</span>
                            </div>
                            <div class="quantity">
                                <span class="amount">{{$details->quantity}}</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-6 cart-action">
                            <div class="s-total">
                                <span class="price">{{$details->price*$details->quantity}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div id="total">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>小計：</td>
                                    <td class="price">
                                        <span>{{$order->total}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>運費：</td>
                                    <td class="price">
                                        <span>{{$order->freight}}</span>
                                    </td>
                                </tr>
                                <tr class="total">
                                    <td>合計：</td>
                                    <td class="price">
                                        <span>{{$order->freight+$order->total}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card mt-4 mb-4">
                        <div class="card-header">
                            訂單資訊
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><span>訂購日期</span>
                                <p>{{date('Y/m/d',strtotime($order->created_at))}}</p>
                            </li>
                            <li class="list-group-item"><span>付款方式</span>
                                @if($order->delivery_method==1)
                                <p>貨到付款</p>
                                @elseif($order->delivery_method==2)
                                <p>線上刷卡</p>
                                @elseif($order->delivery_method==3)
                                <p>銀行匯款</p>
                                @endif
                            </li>
                            <li class="list-group-item"><span>收件地址</span>
                                <p>{{$order->zipcode.' '.$order->county.' '.$order->district.' '.$order->add}}</p>
                            </li>
                            <li class="list-group-item"><span>訂購人姓名</span>
                                <p>{{$order->name}}</p>
                            </li>
                            <li class="list-group-item"><span>訂購人信箱</span>
                                <p>{{$order->email}}</p>
                            </li>
                            <li class="list-group-item"><span>訂購人電話</span>
                                <p>{{$order->tel}}</p>
                            </li>
                            <li class="list-group-item"><span>收件人名稱</span>
                                <p>{{$order->recipient_name}}</p>
                            </li>
                            <li class="list-group-item"><span>收件人信箱</span>
                                <p>{{$order->recipient_email}}</p>
                            </li>
                            <li class="list-group-item"><span>收件人電話</span>
                                <p>{{$order->recipient_tel}}</p>
                            </li>
                            <li class="list-group-item"><span>發票類型</span>
                                @if($order->invoice_method==1)
                                <p>二聯式發票</p>
                                @elseif($order->invoice_method==2)
                                <p>三聯式發票</p>
                                <p>公司抬頭:{{$order->company}}</p>
                                <p>統一編號:{{$order->uniform_numbers}}</p>
                                @endif
                            </li>
                            <li class="list-group-item"><span>備註</span>
                                @if(empty($order->ext))
                                <p>無</p>
                                @else
                                <p>{{$order->ext}}</p>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="{{route('Reception.Member')}}" class="btn btn-outline" >返回會員專區</a>
            </div>
        </div>
    </section>
</div>
    </div>
@stop
@section('script')
<script src="js/jquery.twzipcode.min.js"></script>
<!-- Main JS -->
<script type="text/javascript">
$(document).ready(function() {
    $('#twzipcode').twzipcode({
        'detect': true
    });
    $('#invoice-method').change(function() {
        $('.inv-').hide();
        $('.inv-' + $(this).val()).show("slow");
    }).change();
});
</script>
@stop