@extends('layouts.reception.reception')
@section('style')
    <link rel="stylesheet" href="{{asset('css/products.css')}}">
@stop
@section('content')
<div id="main">
    <div class="products pro">
        <section class="pb-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-end  fadeInLeft" >
                        <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">產品專區</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="container-fluid pl-0 pr-0">
                <div class="tab-content  fadeIn"  id="pro-tab-content">
                    <div class="tab-pane fade show active" id="pro-1" role="tabpanel" aria-labelledby="pro-1-tab">
                        <div class="row no-gutters row-eq-height  fadeIn">
                            @foreach($product as $key =>$val)
                            @if($key<2)
                                <div class="col-md-6 col-12 cover" style="background-image: url({{asset('Upload/Product/'.$val->img1)}});" onclick="javascript:location.href='{{route('Reception.ProductDetail',[$val->id])}}'" title="{{$val->name}}">
                                </div>
                            @elseif($key>=2)
                                <div class="col-md-4 col-6 cover" style="background-image: url({{asset('Upload/Product/'.$val->img1)}});" onclick="javascript:location.href='{{route('Reception.ProductDetail',[$val->id])}}'" title="">
                                </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop
@section('script')
@stop