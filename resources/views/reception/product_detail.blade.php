@extends('layouts.reception.reception')
@section('style')
    <link rel="stylesheet" href="{{asset('css/products.css')}}">
@stop
@section('content')
<div id="main">
    <div class="products pro-de">
    <section class="">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item"><a href="{{route('Reception.Product')}}">產品專區</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">產品應用</a>
                    </li>
                </ol>
            </nav>
        </div>
        <div class="container-fluid pl-0 pr-0">
            <div class="row no-gutters products">
                <div class="col-md-5 col-6 cover wow fadeInLeft" data-wow-duration="2s" style="background-image: url({{asset('Upload/Product/'.$product->img1)}});" title="">
                </div>
                <div class="col-md-7 col-6 text wow fadeIn" data-wow-duration="2s">
                    {!! $product->content !!}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center pro-imgs">
                @if(public_path('Upload/Product/'.$product->img2))
                <div class="col-md-10 col-12 cover wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/Product/'.$product->img2)}});" title="">
                </div>
                @endif
                @if(public_path('Upload/Product/'.$product->img3))
                <div class="col-md-10 col-12 cover wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/Product/'.$product->img3)}});" title="">
                </div>
                @endif
                @if(public_path('Upload/Product/'.$product->img4))
                <div class="col-md-10 col-12 cover wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/Product/'.$product->img4)}});" title="">
                </div>
                @endif
                @if(public_path('Upload/Product/'.$product->img5))
                <div class="col-md-10 col-12 cover wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/Product/'.$product->img5)}});" title="">
                </div>
                @endif
                @if(public_path('Upload/Product/'.$product->img6))
                <div class="col-md-10 col-12 cover wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/Product/'.$product->img6)}});" title="">
                </div>
                @endif
                <div class="col-lg-6 col-md-5 col-4">
                    <a href="{{route('Reception.ProductList')}}" class="w-100 btn btn-primary">
                        立即購買
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
@stop
@section('script')
@stop