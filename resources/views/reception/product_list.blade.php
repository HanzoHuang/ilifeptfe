@extends('layouts.reception.reception')
@section('style')
    <link rel="stylesheet" href="{{asset('css/products.css')}}">
@stop
@section('content')
<div id="main">
    <div class="products pro-list">
        <section class="">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-end wow fadeInLeft" >
                        <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                        <li class="breadcrumb-item"><a href="products.html">產品專區</a></li>
                        <li class="breadcrumb-item">
                            <a href="pro-de.html">產品應用</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">產品列表</a>
                        </li>
                    </ol>
                </nav>
                <div class="row justify-content-start row-eq-height">
                    @foreach($product as $key =>$value)
                    <div class="col-lg-3 col-md-4 col-6 wow fadeIn" >
                        <div class="item">
                            <div class="cover" style="background-image: url({{asset('Upload/Product/'.$value->img1)}});">
                            </div>
                            <div class="text">
                                <h6 class="name">{{$value->name}}{{$value->id}}</h6>
                                <ul>
                                    <li class="price">{{$value->price}}</li>
                                    <li class="size">{{$value->size}}</li>
                                </ul>
                                <!-- <p class="price">750</p> -->
                                <div class="act-btn">
                                    <div class="left count-sel">
                                        <select name="count{{$value->id}}" id="count{{$value->id}}" class="custom-select form-control">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="right">
                                        <input type="button" class="btn btn-primary" value="加入購物車" onclick="add_cart({{$value->id}},{{$value->price}})">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
</div>
@stop
@section('script')
<script type="text/javascript">
    function add_cart(product_id,price) {
        quantity=$('#count'+product_id).find(':selected').val();
        $.ajax({
            url: '{{route('Reception.AddCart')}}',
            data:{product_id:product_id,quantity:quantity,price:price},
            type:"get",
            dataType:'html',
            success: function(data){
                $('#cart_count').html(data);
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('錯誤');
             }
        });
    }
</script>
@stop