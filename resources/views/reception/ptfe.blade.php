@extends('layouts.reception.reception')
@section('style')
        <link rel="stylesheet" href="{{asset('css/products.css')}}">
@stop
@section('content') 
<div id="main">
    <div class="ptfe">
        <section class="pb-0">
            <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">PTFE薄膜</a>
                    </li>
                </ol>
            </nav>
            </div>
            <div class="container-fluid pl-0 pr-0">
                @foreach($ptfe as $key =>$value)
                @if($key%2==0)
                <div class="row no-gutters row-eq-height products wow fadeIn" data-wow-duration="2s">
                    <div class="col-md-6 col-12 overlay">
                        <div class="text">
                            <h5>{{$value->title1}}</h5>
                            <h6>{{$value->title2}}</h6>
                            {!! $value->content !!}
                            <p class="more text-right">
                                <a href="{{route('Reception.PtfeDetail',[$value->id])}}">詳細了解</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 cover" style="background-image: url({{asset('Upload/Ptfe/'.$value->img)}});" title="">
                    </div>
                </div>
                @elseif($key%2==1)
                <div class="row no-gutters row-eq-height products wow fadeIn" data-wow-duration="2s">
                    <div class="col-md-6 col-12 overlay conversely order-sm-2">
                        <div class="text">
                            <h5>{{$value->title1}}</h5>
                            <h6>{{$value->title2}}</h6>
                            {!! $value->content !!}
                            <p class="more text-right">
                                <a href="{{route('Reception.PtfeDetail',[$value->id])}}">詳細了解</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 cover order-sm-1" style="background-image: url({{asset('Upload/Ptfe/'.$value->img)}});" title="">
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </section>
    </div>
</div>
@stop
@section('script')
@stop 