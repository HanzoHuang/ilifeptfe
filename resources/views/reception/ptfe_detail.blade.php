@extends('layouts.reception.reception')
@section('style')
        <link rel="stylesheet" href="{{asset('css/products.css')}}">
@stop
@section('content')   
<div id="main">
    <div class="ptfe ptfe-de">
        <section class="">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                        <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                        <li class="breadcrumb-item"><a href="{{route('Reception.Ptfe')}}">PTFE薄膜</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="#">PTFE薄膜內文</a>
                        </li>
                    </ol>
                </nav>
                <div class="row justify-content-center wow fadeIn" data-wow-duration="2s">
                    <div class="col-lg-10 col-md-11 col-12">
                        <div class="cover" style="background-image: url({{asset('Upload/Ptfe/'.$ptfe->img)}});" title=""></div>
                    </div>
                    <div class="col-lg-10 col-md-11 col-12">
                        <h5>{{$ptfe->title1}}</h5>
                        <h6>{{$ptfe->title2}}</h6>
                        {!! $ptfe->content !!}
                        <div class="act-btn">
                            @if(isset($previous_ptfe))
                            <div class="left">
                                <a href="{{route('Reception.PtfeDetail',[$previous_ptfe->id])}}" class="btn btn-primary">
                                    上一篇
                                </a>
                            </div>
                            @endif
                            @if(isset($next_ptfe))
                            <div class="right">
                                <a href="{{route('Reception.PtfeDetail',[$next_ptfe->id])}}" class="btn btn-primary">
                                    下一篇
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop
@section('script')
@stop 
