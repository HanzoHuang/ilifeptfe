@extends('layouts.reception.reception')
@section('style')
<link rel="stylesheet" href="{{asset('css/about.css')}}">
@stop
@section('content')
    <div id="main" class="content">
        <div class="qna">
    <div class="banner wow fadeIn" data-wow-duration="2s" style="background-image: url({{asset('Upload/QaBanner/'.$qa_banner->img)}});" title="">
    </div>
    <div class="container">
        <section>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-end wow fadeInLeft" data-wow-duration="2s">
                    <li class="breadcrumb-item"><a href="{{route('Home.index')}}">首頁</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="#">常見問題</a>
                    </li>
                </ol>
            </nav>
            <div class="row justify-content-center">
                <div class="col-md-10 col-12 wow fadeInUp" data-wow-duration="2s">
                    <div class="accordion" id="qa">
                        @foreach($qa as $key =>$value)
                        <div class="card">
                            <div class="card-header" id="q-{{$key}}">
                                <h6 class="mb-0" data-toggle="collapse" data-target="#a-{{$key}}" aria-expanded="@if($key==0)true @else false @endif" aria-controls="a-{{$key}}">
                                    {{$value->question}}
                                </h6>
                            </div>
                            <div id="a-{{$key}}" class="collapse show" aria-labelledby="q-{{$key}}" data-parent="#qa">
                                <div class="card-body">
                                   {!!$value->answer!!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
    </div>
@stop
@section('script')
@stop